# How to Run

## Prerequisites
 * Docker
 * Node.js
 * Ubuntu or Mac OS/X

## Steps
 1. Copy the provided __.env__ file to the server root directory.
 2. Define/check the environment variable values as necessary in _.env_ file.
 2. Install _mocha_ using command `npm install -g mocha`
 2. Run the command `sh start.sh --dev`. This should successfully start all the component servers.
 3. Open a new terminal and run `npm test`. All test cases should be passed.
 

