#!/bin/bash

PRODUCTION_PROFILE="production"
if [ ! -z "$1" ]; then
    if [ "$1" = "--dev" ]; then
        PRODUCTION_PROFILE="dev"
    fi
fi

printf "Running in \e[1m$PRODUCTION_PROFILE\e[0m\n"
printf "Checking prerequisites...\n"
echo "-------------------------------------------------\n"

if [ ! -f "./.env" ]; then
    printf "\e[31mEnvironment file not found!\e[0m\n"
    echo ""
    printf "\e[31mCreate a .env file in this working directory with necessary environment variables.\e[0m\n"
    exit 1
fi

export TG_DB_DIR_VOLUME="~/data/mysql/tg"
export TG_PUBLIC_VOLUME="~/data/public"

# create mysql database directory
if [ -d "$TG_DB_DIR_VOLUME" ]; then
    printf "\e[33mCreating database volume directory...\e[0m\n"
    mkdir -p "$TG_DB_DIR_VOLUME"
fi

# create public directory if not exist
if [ -d "$TG_PUBLIC_VOLUME" ]; then
    printf "\e[33mCreating public static file directory...\e[0m\n"
    mkdir -p "$TG_PUBLIC_VOLUME"
fi

printf "\e[1mInitializing server components...\e[0m\n"
echo "-------------------------------------------------\n"

printf "\e[1mStarting Task-Graph server in $PRODUCTION_PROFILE mode...\e[0m\n"
echo "-------------------------------------------------\n"
if [ "$PRODUCTION_PROFILE" = "production" ]; then
    docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml build
    docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up -d
else
    docker-compose -f docker-compose.yaml -f docker-compose.override.yaml up
fi