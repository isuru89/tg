FROM node:6.9.4-alpine

RUN apk update && apk upgrade

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app
RUN npm install --production

COPY . /usr/src/app

# create public static dir
RUN mkdir -p /usr/src/app/public/imgs

EXPOSE 8080

CMD ["node", "lib/server.js"]
