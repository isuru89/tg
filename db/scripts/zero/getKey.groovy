
$DSL.select {
    TARGET (TABLE('user_device').alias('ud'))

    JOIN (ud) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, ud.user
    }

    WHERE {
        EQ (ud.device_id, PARAM('device_id'))
        AND
        EQ (u.mobile_no, PARAM('phone_no'))
        AND
        EQ (ud.is_removed, BOOLEAN(false))
    }
}