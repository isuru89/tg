
$DSL.script {

    System.out.println($SESSION.tables);
    def tables = $SESSION.tables ?: [];

    for (def table : tables) {
        def q = $DSL.delete { TARGET(TABLE(table).alias("t")) };
        System.out.println('Truncating table ' + table + '...')
        RUN(q)
    }

    return [];
}