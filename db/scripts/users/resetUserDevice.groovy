
$DSL.update {
    TARGET (TABLE('user_device').alias('ud'))

    JOIN (ud) {
        INNER_JOIN (TABLE('user').alias('u')) ON ud.user, u.user_id
    }

    WHERE {
        EQ (u.mobile_no, PARAM('user_phone'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
        AND
        EQ (ud.reset_token, PARAM('reset_token'))
        AND
        GT (ud.reset_expires, PARAM('current_time'))
    }

    SET {
        EQ (ud.is_activated, BOOLEAN(false))
        EQ (ud.is_removed, BOOLEAN(true))
    }
}
