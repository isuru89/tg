
$DSL.select {

    TARGET (TABLE("user").alias("u"))

    WHERE {
        EQ (u.user_id, PARAM("user_id"))
    }

    LIMIT 1

}