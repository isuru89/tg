
$DSL.select {

    TARGET (TABLE("user").alias("u"))

    WHERE {
        EQ (u.mobile_no, PARAM("user_phone"))
    }

    LIMIT 1

}