
$DSL.insert {
    TARGET (TABLE("user_device").alias("ud"))

    DATA ([
        user: PARAM("user_id"),
        device_id: PARAM("device_id"),
        activation_code: PARAM("activation_code"),
        created_at: PARAM("current_time"),
        is_removed: BOOLEAN(false)
    ])
}