
$DSL.insert {

    TARGET (TABLE("user").alias("u"))

    DATA ([
        mobile_no: PARAM("user_phone"),
        created_at: PARAM("current_time")
    ])

    RETURN_KEYS()
}