
def uq = $DSL.update {
    TARGET (TABLE('user_device').alias('ud'))

    WHERE {
        EQ (ud.user,    PARAM('user_id'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
    }

    SET {
        EQ (ud.reset_token, PARAM('reset_token'))
        EQ (ud.reset_expires, PARAM('reset_expires'))
    }
}

def rd = $DSL.select {
    TARGET (TABLE('user_device').alias('ud'))

    WHERE {
        EQ (ud.user,    PARAM('user_id'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
    }

    FETCH (ud.reset_token)
    
    LIMIT 1
}

$DSL.script {

    def rtoken = java.util.UUID.randomUUID() as String;
    $SESSION.reset_token = rtoken;
    $SESSION.reset_expires = $SESSION.current_time + (24 * 60 * 60 * 1000);

    RUN (uq);
    def readRes = RUN (rd);

    return readRes.getField(0, 'reset_token')

}