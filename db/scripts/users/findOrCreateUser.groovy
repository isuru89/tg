/// Parameters:
///     - user_id:
///     - user_phone:
///     - device_id:
///

$DSL.script {

    if ($SESSION.containsKey('user_id')) {
        def theUser = RUN('users/getUserById')
        return [temporary: false, user: theUser[0]]

    } else if ($SESSION.containsKey('user_phone')) {
        def theDevices = RUN('users/allDevicesOfUser')

        // there is a user and at least one device
        if (theDevices != null && theDevices.size() > 0) {
            def userDevice = null
            def uid = theDevices.getField(0, 'user_id')
            for (def uDevice : theDevices) {
                if (uDevice.device_id == $SESSION.device_id) {
                    userDevice = uDevice
                    break
                }
            }

            $SESSION.user_id = uid
            def usrObj = RUN('users/getUserById')[0]
            $SESSION.activation_code = java.util.concurrent.ThreadLocalRandom.current().nextInt(100000, 999999)

            if (userDevice != null) {
                // if (userDevice.is_activated == true) {
                //     def rToken = RUN ('users/addResetToken');
                //     return [temporary: false, user: usrObj, resetToken: rToken]
                // } else {
                //     return [temporary: true, user: usrObj, activationCode: $SESSION.activation_code];
                // }
                RUN ('users/updateCodeForUser');
                return [temporary: true, user: usrObj, activationCode: $SESSION.activation_code];

            } else {
                // there is no device for user.
                RUN("users/createDeviceForUser")
                return [temporary: true, user: usrObj, activationCode: $SESSION.activation_code]
            }
            
        } else {
            // no user, let's create temp one.
            $SESSION.activation_code = java.util.concurrent.ThreadLocalRandom.current().nextInt(100000, 999999)
            
            def createResult = RUN('users/createTempUser')
            def temp_user_id = createResult.affectedKeys()[0]

            $SESSION.user_id = temp_user_id
            RUN('users/createDeviceForUser')
            def defBoard = RUN('lists/createDefaultList').affectedKeys()[0]
            
            return [temporary: true, user: RUN('users/getUserById')[0], activationCode: $SESSION.activation_code]
        }
        
    } else {
        return []
    }

}