
$DSL.update {
    TARGET (TABLE('user_device').alias('ud'))

    WHERE {
        EQ (ud.user, PARAM('user_id'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
    }

    SET {
        EQ (ud.token, PARAM('token'))
    }
}