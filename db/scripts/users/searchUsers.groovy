$DSL.select {
    TARGET (TABLE('user').alias('u'))

    WHERE {
        LIKE (u.first_name, PARAM('name'))
    }
}