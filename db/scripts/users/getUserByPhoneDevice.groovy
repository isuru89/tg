/// Parameters:
///     - phone_no:
///     - device_id:
///

$DSL.select {

    TARGET (TABLE('user').alias('u'))

    JOIN (u) {
        INNER_JOIN (TABLE('user_device').alias('ud')) ON u.user_id, ud.user
    }

    WHERE {
        EQ (u.mobile_no, PARAM('phone_no'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
        AND
        EQ (ud.is_removed, BOOLEAN(false))
    }

    LIMIT 1

}