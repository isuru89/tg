
$DSL.script {

    def q = $DSL.select {
        TARGET (TABLE('user').alias('u'))
        FETCH (u.user_id, u.is_temporary)
        WHERE {
            EQ (u.mobile_no, PARAM('user_phone'))
        }
        LIMIT 1
    };

    def countRes = RUN (q);

    if (countRes.size() > 0) {
        // a user exist...
        // no need to send invitation...
        return [userId: countRes[0].user_id, isTemp: countRes[0].is_temporary];

    } else {
        $SESSION.current_time = System.currentTimeMillis();

        def addedUserId = RUN ('users/createTempUser').affectedKeys()[0];
        return [userId: addedUserId, isTemp: true]
    }

}