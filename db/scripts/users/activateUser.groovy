/// Parameters:
///     - phone_no
///     - device_id
///     - activated_time

def updateUser = $DSL.update {

    TARGET (TABLE('user_device').alias('ud'))

    JOIN (ud) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, ud.user
    }

    WHERE {
        EQ (u.mobile_no, PARAM('phone_no'))
        AND
        EQ (ud.device_id, PARAM('device_id'))
        AND
        EQ (ud.is_removed, BOOLEAN(false))
    }

    SET {
        EQ (ud.is_activated, BOOLEAN(true))
        EQ (u.is_temporary, BOOLEAN(false))
        EQ (ud.activated_at, PARAM('activated_time'))
    }

}

def findDefBoard = $DSL.select {
    TARGET (TABLE('list').alias('l'))

    JOIN (l) {
        INNER_JOIN (TABLE('user').alias('u')) ON l.owner, u.user_id
    }

    FETCH (l.list_id)

    WHERE {
        EQ (l.is_default, BOOLEAN(true))
        AND
        EQ (u.mobile_no, PARAM('phone_no'))
    }

    LIMIT 1
}

$DSL.script {

    def res = RUN(updateUser);

    if (res.affectedCount() > 0) {
        def dBoard = RUN(findDefBoard).getField(0, 'list_id');
        return [success: true, defaultBoard: dBoard]
    } else {
        return [success: false, defaultBoard: -1]
    }

}