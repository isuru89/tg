
def iq = $DSL.bulkInsert {
    TARGET (TABLE("_tmp_phones").alias("tp"))

    DATA ([
        mobile_no: PARAM('phone_no')
    ])
}

def sq = $DSL.select {

    TARGET (TABLE("_tmp_phones").alias("tp"))

    JOIN (tp) {
        LEFT_JOIN (TABLE('user').alias('u')) ON u.mobile_no, tp.mobile_no
    }

    FETCH (tp.mobile_no.alias("phone"), IFNULL(u.user_id, -1).alias("user_id"))

}

$DSL.script {

    RUN($DSL.ddl {
        TEMP_TABLE ('_tmp_phones', true) {
            FIELD("mobile_no", DFieldType.TEXT, [notNull: false, length: 24])
        }
    });

    RUN (iq);

    def result = RUN (sq);

    RUN($DSL.ddl {
        DROP_TEMP_TABLE ('_tmp_phones')
    });

    return result;

}

