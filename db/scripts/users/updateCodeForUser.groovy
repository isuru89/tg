
$DSL.update {
    TARGET (TABLE("user_device").alias("ud"))

    SET {
        EQ (ud.activation_code,    PARAM('activation_code'))
    }

    WHERE {
        EQ (ud.user,    PARAM('user_id'))
        AND
        EQ (ud.device_id,   PARAM('device_id'))
    }
}