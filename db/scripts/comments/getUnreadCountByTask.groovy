/// Parameters:
///     - user_id
///

@Field do_cache = true

$DSL.select {

    TARGET (TABLE("user_comment_read").alias("ucr"))

    JOIN (ucr) {
        INNER_JOIN (TABLE("comment").alias("c")) ON c.task, ucr.task
        INNER_JOIN (TABLE('task').alias('t')) ON ucr.task, t.task_id
    }

    FETCH (c.task.alias('task_id'), COUNT().alias("unreadCount"))

    WHERE {
        EQ (ucr.user, PARAM('user_id'))
        AND
        GT (c.posted_at, ucr.last_read)
        AND
        EQ (t.is_active, BOOLEAN(true))
    }

    GROUP_BY (c.task)

}