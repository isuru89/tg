/// Parameters:
///     - id
///

@Field do_cache = true

$DSL.select {

    TARGET(TABLE('comment').alias("c"))

    FETCH (c.task, c.message, c.posted_at)

    WHERE {
        EQ (e.comment_id, PARAM("id"))
    }
}