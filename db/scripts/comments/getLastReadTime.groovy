
$DSL.select {

    TARGET (TABLE("user_comment_read").alias("ucr"))

    WHERE {
        EQ (ucr.user, PARAM("user_id"))
        AND
        EQ (ucr.task, PARAM("task_id"))
    }

    FETCH (ucr.last_read)

}