/// Parameters:
///     - user_id
///     - task_id
///

@Field do_cache = true

$DSL.select {

    TARGET (TABLE("user_comment_read").alias("ucr"))

    JOIN (ucr) {
        INNER_JOIN (TABLE("comment").alias("c")) ON c.task, ucr.task
    }

    FETCH (COUNT().alias("unreadCount"))

    WHERE {
        EQ (ucr.user, PARAM('user_id'))
        AND
        EQ (c.task, PARAM('task_id'))
        AND
        GT (c.posted_at, ucr.last_read)
    }

}