/// Parameters:
///     - user_id
///     - task_id
///     - last_read_time
///

@Field do_cache = true

$DSL.update {

    TARGET (TABLE("user_comment_read").alias("ucr"))

    WHERE {
        EQ (ucr.user, PARAM("user_id"))
        AND
        EQ (ucr.task, PARAM("task_id"))
    }

    SET {
        EQ (ucr.last_read, PARAM("last_read_time"))
    }

}