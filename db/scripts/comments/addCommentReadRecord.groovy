
$DSL.insert {
    TARGET (TABLE('user_comment_read').alias('ucr'))

    DATA ([
        user: PARAM('user_id'),
        task: PARAM('task_id'),
        last_read: PARAM('last_read_time')
    ])

}