
$DSL.script {
    
    // remove list record
    def delResult = RUN("lists/remove/removeListRecord")

    if (delResult.affectedCount() > 0) {
        // remove shared users
        RUN("lists/remove/removeListSharing")

        // remove list
        RUN('lists/remove/removeListTasks')
    } 

    return delResult
}