
def delQ = $DSL.delete {

    TARGET (TABLE("list_sharing").alias("ls"))

    JOIN (ls) {
        INNER_JOIN (TABLE('list').alias('l')) ON ls.list, l.list_id
    }

    WHERE {
        EQ (l.list_id, PARAM("list_id"))
        AND
        EQ (ls.shared_with, PARAM("remove_user"))
        AND
        ANY {
            EQ (l.owner, PARAM('user_id'))
            EQ (ls.shared_with, PARAM('user_id'))
        }
    }
}

$DSL.script {

    def result = RUN(delQ);
    def userCount = RUN('lists/getSharedUserCount').getField(0, 'total');
    if (userCount <= 0) {
        $SESSION.shared_status = false;
        RUN ('lists/updateSharedStatus');
    }
    return result;

}