
$DSL.delete {
    TARGET (TABLE("list_sharing").alias("ls"))
    WHERE {
        EQ (ls.list, PARAM("list_id"))
    }
}