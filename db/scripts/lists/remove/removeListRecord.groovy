
$DSL.delete {

    TARGET (TABLE("list").alias("l"))

    WHERE {
        EQ (l.list_id, PARAM("list_id"))
        AND
        EQ (l.is_default, BOOLEAN(false))
        AND
        EQ (l.owner, PARAM('user_id'))
    }
}