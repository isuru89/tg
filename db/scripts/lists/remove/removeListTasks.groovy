
$DSL.delete {
    TARGET (TABLE('list_task').alias('lt'))

    WHERE {
        EQ (lt.list, PARAM('list_id'))
    }
}