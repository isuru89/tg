
// select my own lists
def myLists = $DSL.select {

    TARGET (TABLE("list").alias("l"))

    WHERE {
        EQ (l.owner, PARAM("user_id"))
    }

    FETCH (l)
}

// select lists shared with me by other people
def sharedLists = $DSL.select {

    TARGET(TABLE("list_sharing").alias("ls"))

    JOIN (ls) {
        INNER_JOIN (TABLE("list").alias("l")) ON l.list_id, ls.list
    }

    FETCH (l)

    WHERE {
        EQ (ls.shared_with, PARAM("user_id"))
    }

}

$DSL.unionDistinct(myLists, sharedLists)