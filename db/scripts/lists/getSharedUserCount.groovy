
$DSL.select {
    TARGET (TABLE('list_sharing').alias('ls'))

    FETCH (COUNT().alias('total'))

    WHERE {
        EQ (ls.list, PARAM('list_id'))
    }
}