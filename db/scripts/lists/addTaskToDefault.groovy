// task_id
// to_user
//

$DSL.insert {
    TARGET (TABLE('list').alias('l'))

    JOIN (l) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, l.owner
    }

    FETCH (l.list_id.alias('list'), PARAM('task_id').alias('task'))

    INTO (TABLE('list_task').alias('lt'), lt.list, lt.task)

    WHERE {
        EQ (u.user_id, PARAM('to_user'))
        AND
        EQ (l.is_default, BOOLEAN(true))
    }
}