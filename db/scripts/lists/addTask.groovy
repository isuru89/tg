/// Parameters:
///     - task_id
///     - list_id
///

@Field do_cache = true

$DSL.insert {
    TARGET (TABLE('list_task').alias('lt'))

    DATA ([
        task:   PARAM('task_id'),
        list:   PARAM('list_id')
    ])
}