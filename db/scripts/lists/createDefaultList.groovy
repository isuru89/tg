
$DSL.insert {

    TARGET (TABLE("list").alias("l"))

    DATA ([
        title:          STR("default"),
        is_default:     BOOLEAN(true),
        owner:          PARAM("user_id"),
        created_at:     PARAM("current_time")
    ])

    RETURN_KEYS()
}