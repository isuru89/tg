
$DSL.update {
    TARGET (TABLE('list').alias('l'))

    SET {
        EQ (l.is_shared, PARAM('shared_status'))
    }

    WHERE {
        EQ (l.list_id, PARAM('list_id'))
    }
}