
def batchQ = $DSL.bulkInsert {

    TARGET (TABLE("list_sharing").alias("ls"))

    DATA ([
        list: PARAM("list_id"),
        shared_with: PARAM("user_id")
    ])

}

$DSL.script {

    def result = RUN(batchQ);
    RUN ('lists/updateSharedStatus');
    
    return result;

}