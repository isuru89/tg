/// Parameters:
///     - list_id
///     - user_id
/// 


$DSL.select {

    TARGET (TABLE("list_sharing").alias("ls"))

    JOIN (ls) {
        INNER_JOIN (TABLE("user").alias("u")) ON ls.shared_with, u.user_id
        INNER_JOIN (TABLE('list').alias('l')) ON l.list_id, ls.list
    }

    FETCH (
        u.user_id, 
        u.mobile_no.alias('phone_no'),
        u.first_name,
        u.last_name
    )

    WHERE {
        EQ (ls.list, PARAM("list_id"))
        AND
        ANY {
            EQ (l.owner, PARAM('user_id'))
            EQ (u.user_id, PARAM('user_id'))
        }
    }
}