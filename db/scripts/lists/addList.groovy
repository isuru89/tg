/// Parameters:
///     - title
///     - user_id
///     - time
///
/// NOTE: DON'T CACHE THIS SCRIPT
///

$DSL.insert {
    TARGET (TABLE('list').alias('l'))

    DATA ([
        title:      PARAM('title'),
        owner:      PARAM('user_id'),
        created_at: PARAM('time')
    ])

    RETURN_KEYS()
}