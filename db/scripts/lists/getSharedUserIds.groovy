def q1 = $DSL.select {
    TARGET (TABLE('list_sharing').alias('ls'))
    DISTINCT_FETCH (ls.shared_with.alias('user'))
    WHERE {
        EQ (ls.list, PARAM('list_id'))
    }
}

def q2 = $DSL.select {
    TARGET (TABLE('list').alias('l'))
    FETCH (l.owner.alias('user'))
    WHERE {
        EQ (l.list_id, PARAM('list_id'))
    }
}

$DSL.union(q1, q2);