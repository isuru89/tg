
$DSL.select {

    TARGET (TABLE("task_sharing").alias("ts"))

    JOIN (ts) {
        INNER_JOIN (TABLE("user").alias("u")) ON u.user_id, ts.share_with
    }

    FETCH (u.user_id, u.mobile_no.alias('phone_no'))

    WHERE {
        EQ (ts.task, PARAM("task_id"))
    }

}