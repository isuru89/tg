/// Parameters:
///     - task_id
///

@Field do_cache = true

$DSL.select {
    TARGET (TABLE('task').alias('t'))

    JOIN (t) {
        LEFT_JOIN (TABLE('user').alias('u1')) ON t.assigned_to, u1.user_id
        INNER_JOIN (TABLE('user').alias('u2')) ON t.created_by, u2.user_id
    }

    FETCH (
        t.task_id,
        t.title,
        t.description,
        t.status,
        t.assigned_to,
        u1.mobile_no.alias('assigned_to_user_phone'),
        CONCAT(u1.first_name, STR(' '), u1.last_name).alias('assigned_to_user'),
        t.due_start,
        t.due_end,
        t.created_by,
        u2.mobile_no.alias('created_by_user_phone'),
        CONCAT(u2.first_name, STR(' '), u2.last_name).alias('created_by_user'),
        t.created_at,
        TABLE($IMPORT('tasks/getChildCount')).alias("childCount")
    )

    WHERE {
        EQ (t.task_id, PARAM('task_id'))
    }
}