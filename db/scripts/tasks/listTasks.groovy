/// Parameters:
///     - list_id
///     - user_id

@Field do_cache = true

def myListTasks = $DSL.select {

    TARGET (TABLE("list").alias("l"))

    JOIN (l) {
        INNER_JOIN (TABLE('list_task').alias('lt')) ON l.list_id, lt.list
        INNER_JOIN (TABLE('task').alias('t')) ON lt.task, t.task_id
        LEFT_JOIN (TABLE('list_sharing').alias('ls')) ON ls.list, l.list_id
        LEFT_JOIN (TABLE('task_tree').alias('tt')) ON t.task_id, tt.child_task
    }

    FETCH (t, tt)

    WHERE {
        EQ (l.list_id, PARAM("list_id"))
        AND
        EQ (t.is_active, BOOLEAN(true))
        AND
        ANY {
            EQ (l.owner, PARAM('user_id'))
            EQ (ls.shared_with, PARAM('user_id'))
        }
        AND
        IN (t.status, PARAMLIST('status'))
    }

    ORDER_BY (DESC(t.created_at))

}

def childCountQ = $DSL.select {

    EXPECT (TABLE('task').alias('myTasks'))

    TARGET (TABLE("task_tree").alias("ctt"))

    JOIN (ctt) {
        INNER_JOIN (TABLE("task").alias("ct")) ON ct.task_id, ctt.child_task
    }

    FETCH (COUNT().alias("subTaskCount"))

    WHERE {
        EQ (ctt.this_task, myTasks.task_id)
        AND
        EQ (ct.is_active, BOOLEAN(true))
    }

}

$DSL.select {
    TARGET (TABLE(myListTasks).alias("myTasks"))

    JOIN (myTasks) {
        LEFT_JOIN (TABLE(myListTasks).alias("myTasks2")) ON myTasks.this_task, myTasks2.task_id
        LEFT_JOIN (TABLE('user').alias('u1')) ON myTasks.assigned_to, u1.user_id
        INNER_JOIN (TABLE('user').alias('u2')) ON myTasks.created_by, u2.user_id
    }

    DISTINCT_FETCH (
        myTasks.task_id,
        myTasks.title,
        myTasks.description,
        myTasks.status,
        myTasks.assigned_to,
        u1.mobile_no.alias('assigned_to_user_phone'),
        CONCAT(u1.first_name, STR(' '), u1.last_name).alias('assigned_to_user'),
        myTasks.due_start,
        myTasks.due_end,
        myTasks.created_by,
        u2.mobile_no.alias('created_by_user_phone'),
        CONCAT(u2.first_name, STR(' '), u2.last_name).alias('created_by_user'),
        myTasks.created_at,
        TABLE(childCountQ).alias("childCount")
    )

    WHERE {
        EQ (myTasks2.task_id, null)
    }


}