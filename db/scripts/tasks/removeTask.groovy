/// Parameters:
///     - task_id
///     - user_id
///

@Field do_cache = true

$DSL.update {
    TARGET (TABLE('task').alias('t'))

    WHERE {
        EQ (t.task_id, PARAM('task_id'))
        AND
        ANY {
            EQ (t.assigned_to, PARAM('user_id'))
            EQ (t.created_by, PARAM('user_id'))
            LT (t.assigned_to, NUM(0))
        }
    }

    SET {
        EQ (t.is_active, BOOLEAN(false))
    }
}