/// Parameters:
///     - task_id
///     - limit
///     - offset
///

@Field do_cache = true

$DSL.select {

    TARGET(TABLE("comment").alias("c"))

    FETCH (c.message, c.posted_at, c.task)

    WHERE {
        EQ (c.task, PARAM("task_id"))
    }

    ORDER_BY (DESC(c.posted_at))

    LIMIT (PARAM("limit"))
    OFFSET (PARAM("offset"))
}