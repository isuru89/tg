
$DSL.select {
    TARGET (TABLE('list_task').alias('lt'))
    FETCH (lt.list)
    WHERE {
        EQ (lt.task, PARAM('task_id'))
    }
}