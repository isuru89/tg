/// Parameters:
///     - task_id
///

$DSL.script {

    def tasks = RUN ('tasks/readTask')
    if (tasks.size() > 0) {
        if (tasks.getField(0, 'childCount') > 0) {
            def subtasks = RUN ('tasks/getChildTasks')
            tasks[0].put('children', subtasks);
        } else {
            tasks[0].put('children', []);
        }
        tasks[0].put('sharedUsers', RUN('tasks/getSharedUsers'));
    }
    return tasks;

}