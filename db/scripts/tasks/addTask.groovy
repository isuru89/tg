// Parameters
//      - list_id
//      - task_id
//      - data
//      - parent_task
//

def q = $DSL.select {
            TARGET (TABLE('task').alias('t'))
            FETCH (t.assigned_to)
            WHERE {
                EQ (t.task_id, PARAM('parent_task'))
            }
            LIMIT 1
        };

$DSL.script {

    boolean hasParent = $SESSION.containsKey('parent_task') && $SESSION.parent_task > 0;
    // if (hasParent) {
    //     def pTasks = RUN (q);

    //     // check whether parent task assignee is equal to the creator of this task
    //     //if (pTasks.size() <= 0 || pTasks[0].assigned_to != $SESSION.user_id) {
    //         // does not have permission to add
    //         // return [[count: 0], [keys: []]]
    //     //}
    // }

    def addTaskId = RUN ('breads/add').affectedKeys()[0];
    $SESSION.task_id = addTaskId;

    // add to created user list
    def listRes = RUN ('lists/addTask');
    
    $SESSION.last_read_time = $SESSION.data.created_at;
    RUN ('comments/addCommentReadRecord');

    // add as a child task
    if (hasParent) {
        RUN ('tasks/addSubTask');
    }

    // add to assigned_to user's default list, if not person himself.
    if ($SESSION.data.containsKey('assigned_to') && $SESSION.user_id != $SESSION.data.assigned_to) {
        $SESSION.to_user = $SESSION.data.assigned_to;
        RUN ('lists/addTaskToDefault')
    }

    return [[count: 1], [keys: [addTaskId]]];

}