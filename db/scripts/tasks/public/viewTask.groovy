
$DSL.select {
    TARGET (TABLE('task_temp_token').alias('ttt'))
    
    WHERE {
        ALL {
            EQ (ttt.task, PARAM('task_id'))
            EQ (ttt.token, PARAM('token'))
            EQ (ttt.user, PARAM('user_id'))
        }
    }
}