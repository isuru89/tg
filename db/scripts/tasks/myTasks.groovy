
def pastQuery = $DSL.select {
    TARGET (TABLE('task').alias('t'))

    JOIN (t) {
        INNER_JOIN (TABLE('user').alias('u')) ON t.created_by, u.user_id
        LEFT_JOIN (TABLE('user').alias('u2')) ON t.assigned_to, u2.user_id
    }

    FETCH (
        t.task_id,
        t.title,
        t.due_start,
        t.due_end,
        t.created_at,
        t.created_by.alias('created_user_id'),
        CONCAT(u.first_name, STR(' '), u.last_name).alias('created_user'),
        u.mobile_no.alias('created_user_phone'),
        t.assigned_to.alias('assigned_user_id'),
        CONCAT(u2.first_name, STR(' '), u2.last_name).alias('assigned_user'),
        u2.mobile_no.alias('assigned_user_phone'),
        t.status,
    )

    WHERE {
        ANY {
            EQ (t.assigned_to, PARAM('user_id'))
            EQ (t.created_by, PARAM('user_id'))
        }
        AND
        EQ (t.is_active, BOOLEAN(true))
        AND
        NOTNULL (t.due_start)
        AND
        LTE (t.due_start, PARAM('base_time'))
        AND
        IN (t.status, PARAMLIST('status'))
    }

    ORDER_BY (DESC(t.due_start), DESC(t.created_at))

    OFFSET (PARAM('offset'))
    LIMIT (PARAM('limit'))
}

def futureQuery = $DSL.select {
    TARGET (TABLE('task').alias('t'))

    JOIN (t) {
        INNER_JOIN (TABLE('user').alias('u')) ON t.created_by, u.user_id
        LEFT_JOIN (TABLE('user').alias('u2')) ON t.assigned_to, u2.user_id
    }

    FETCH (
        t.task_id,
        t.title,
        t.due_start,
        t.due_end,
        t.created_at,
        t.created_by.alias('created_user_id'),
        CONCAT(u.first_name, STR(' '), u.last_name).alias('created_user'),
        u.mobile_no.alias('created_user_phone'),
        t.assigned_to.alias('assigned_user_id'),
        CONCAT(u2.first_name, STR(' '), u2.last_name).alias('assigned_user'),
        u2.mobile_no.alias('assigned_user_phone'),
        t.status,
    )

    WHERE {
        ANY {
            EQ (t.assigned_to, PARAM('user_id'))
            EQ (t.created_by, PARAM('user_id'))
        }
        AND
        EQ (t.is_active, BOOLEAN(true))
        AND
        NOTNULL (t.due_start)
        AND
        GTE (t.due_start, PARAM('base_time'))
        AND
        IN (t.status, PARAMLIST('status'))
    }

    ORDER_BY (t.due_start, t.created_at)

    OFFSET (PARAM('offset'))
    LIMIT (PARAM('limit'))
}

def bothQuery = $DSL.unionDistinct(pastQuery, futureQuery);

$DSL.script {

    if ($SESSION.direction < 0) {
        return RUN(pastQuery);
    } else if ($SESSION.direction > 0) {
        return RUN(futureQuery);
    } else {
        return RUN(bothQuery);
    }

}