
def q = $DSL.insert {
    TARGET (TABLE('task_temp_token').alias('ttt'))

    DATA ([
        task:       PARAM('task_id'),
        token:      PARAM('token'),
        user:       PARAM('user_id'),
        expire_when: PARAM('expire_when')
    ])
}


def sq = $DSL.select {
    TARGET (TABLE('task_temp_token').alias('ttt'))
    JOIN (ttt) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, ttt.user
    }
    FETCH (ttt.token, u.mobile_no.alias('phone_no'))
    WHERE {
        ALL {
            EQ (ttt.task, PARAM('task_id'))
            EQ (ttt.token, PARAM('token'))
            EQ (ttt.user, PARAM('user_id'))
        }
    }
    LIMIT 1
}

$DSL.script {

    RUN(q);
    return RUN (sq);

}