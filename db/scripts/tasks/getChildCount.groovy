/// Parameters:
///     - task_id
///

@Field do_cache = true

$DSL.select {

    EXPECT (TABLE('task').alias('t'))

    TARGET (TABLE("task_tree").alias("ctt"))

    JOIN (ctt) {
        INNER_JOIN (TABLE("task").alias("ct")) ON ct.task_id, ctt.child_task
    }

    FETCH (COUNT().alias("subTaskCount"))

    WHERE {
        EQ (ctt.this_task, t.task_id)
        AND
        EQ (ct.is_active, BOOLEAN(true))
    }

}