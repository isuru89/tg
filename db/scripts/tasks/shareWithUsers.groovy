/// Parameters:
///     - task_id
///     - user_id
///

@Field do_cache = true

$DSL.bulkInsert {

    TARGET (TABLE("task_sharing").alias("ts"))

    DATA([
        task:       PARAM("task_id"),
        share_with: PARAM("user_id")
    ])

}