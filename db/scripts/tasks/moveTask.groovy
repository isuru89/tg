/// Parameters:
///     - task_id
///     - from_list
///     - to_list
///

@Field do_cache = true

$DSL.update {
    TARGET (TABLE('list_task').alias('lt'))

    WHERE {
        EQ (lt.list, PARAM('from_list'))
        AND
        EQ (lt.task, PARAM('task_id'))
    }

    SET {
        EQ (lt.list, PARAM('to_list'))
    }
}