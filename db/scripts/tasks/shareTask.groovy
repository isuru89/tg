
$DSL.script {

    def result = RUN ('tasks/shareToDefault')
    if (result.affectedCount() > 0) {
        RUN ('tasks/shareWithUsers')
        RUN ('comments/addCommentReadRecordBulk')
    }

    return result;
}