
$DSL.select {

    TARGET (TABLE("task_tree").alias("tt"))

    JOIN (tt) {
        INNER_JOIN (TABLE("task").alias("t")) ON t.task_id, tt.child_task
    }

    FETCH (t.task_id,
        t.title,
        t.description,
        t.status,
        t.assigned_to,
        t.due_start,
        t.due_end,
        t.created_by,
        t.created_at,
        TABLE($IMPORT('tasks/getChildCount')).alias("childCount"))

    WHERE {
        EQ (tt.this_task, PARAM('task_id'))
        AND
        EQ (t.is_active, BOOLEAN(true))
    }

    ORDER_BY (DESC(t.created_at))

}