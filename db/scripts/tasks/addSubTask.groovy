// Parameters
//      - parent_task
//      - child_task
//

def iq = $DSL.insert {
    TARGET (TABLE('task_tree').alias('tt'))

    DATA ([
        this_task:  PARAM('parent_task'),
        child_task: PARAM('task_id')
    ])

}

$DSL.script {

    RUN (iq);

}