
def data = $SESSION.data as Map
def only = $SESSION._only ?: []
boolean goingToAssign = data.containsKey('assigned_to');
def q1 = $DSL.select {
    TARGET (TABLE('list_sharing').alias('ls'))
    DISTINCT_FETCH (ls.shared_with.alias('user'))
    WHERE {
        EQ (ls.list, PARAM('list_id'))
        AND
        EQ (ls.shared_with, PARAM('user_id'))
    }
}

def q2 = $DSL.select {
    TARGET (TABLE('list').alias('l'))
    FETCH (l.owner.alias('user'))
    WHERE {
        EQ (l.list_id, PARAM('list_id'))
        AND
        EQ (l.owner, PARAM('user_id'))
    }
}

def unionq = $DSL.union(q1, q2);

def uq = $DSL.update {

    TARGET(TABLE("task").alias("t"))

    WHERE {
        EQ (t.task_id, PARAM("id"))
        AND
        ANY {
            EQ (t.assigned_to, PARAM("user_id"))
            EQ (t.created_by, PARAM("user_id"))
            EXISTS (unionq)
            LT (t.assigned_to, NUM(0))
        }
    }

    SET {
        for (def entry : data) {
            String col = entry.key
            
            if (only.contains(col)) {
                EQ (t.COLUMN(col), PARAM("data." + col))
            }
        }
    }
}

$DSL.script {

    boolean selfAssign = (data.assigned_to == $SESSION.user_id);
    def result = RUN (uq);

    if (result.affectedCount() > 0 && data.assigned_to != null && !selfAssign) {
        $SESSION.to_user = data.assigned_to;
        $SESSION.task_id = $SESSION.id;
        def res = RUN ('lists/addTaskToDefault');
    } 

    return result;

}