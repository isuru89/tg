
$DSL.select {
    TARGET (TABLE('task_temp_token').alias('ttt'))
    JOIN (ttt) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, ttt.user
    }
    
    WHERE {
        ALL {
            EQ (ttt.task, PARAM('task_id'))
            EQ (ttt.token, PARAM('token'))
            EQ (ttt.user, PARAM('user_id'))
        }
    }
    LIMIT 1
}