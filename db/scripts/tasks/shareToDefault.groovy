/// Paramters:
///     - all_users
///     - task_id
///

$DSL.select {

    TARGET (TABLE('list').alias('l'))

    JOIN (l) {
        INNER_JOIN (TABLE('user').alias('u')) ON u.user_id, l.owner
    }

    FETCH (l.list_id.alias('list'), PARAM('task_id').alias('task'))

    INTO (TABLE('list_task').alias('lt'), lt.list, lt.task)

    WHERE {
        EQ (l.is_default, BOOLEAN(true))
        AND
        IN (u.user_id, PARAMLIST('all_users'))
    }

}