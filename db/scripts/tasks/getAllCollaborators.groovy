
def q1 = $DSL.select {
    TARGET (TABLE('task_sharing').alias('ts'))

    FETCH (ts.share_with.alias('user'))

    WHERE {
        EQ (ts.task, PARAM('task_id'))
    }
}

def q2 = $DSL.select {
    TARGET (TABLE('task').alias('t'))
    FETCH (t.created_by.alias('user'))
    WHERE {
        EQ (t.task_id, PARAM('task_id'))
    }
}

def q3 = $DSL.select {
    TARGET (TABLE('task').alias('t'))
    FETCH (t.assigned_to.alias('user'))
    WHERE {
        EQ (t.task_id, PARAM('task_id'))
    }
}

$DSL.unionDistinct(q2, q3, q1);
