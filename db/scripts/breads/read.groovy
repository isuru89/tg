
$DSL.select {

    TARGET(TABLE($SESSION.entity).alias("e"))

    if ($SESSION.containsKey('columns')) {
        for (def entry : $SESSION.columns) {
            FETCH (e.COLUMN(entry.toString()))
        }
    } else {
        FETCH (e.COLUMN($SESSION.entity + "_id"))
    }

    WHERE {
        EQ (e.COLUMN($SESSION.entity + "_id"), PARAM("id"))
    }
}