
def mapping = [ EQ: QOperator.EQUAL,
                NEQ: QOperator.NOT_EQUAL,
                GT: QOperator.GREATER_THAN,
                GTE: QOperator.GREATER_THAN_EQUAL,
                LT: QOperator.LESS_THAN,
                LTE: QOperator.LESS_THAN_EQUAL
              ]

$DSL.select {

    TARGET(TABLE($SESSION.entity).alias("e"))

    if ($SESSION.containsKey('columns')) {
        for (def entry : $SESSION.columns) {
            FETCH (e.COLUMN(entry.toString()))
        }
    }

    if ($SESSION.containsKey('where')) {
        Map conditions = $SESSION.where

        WHERE {
            ALL {

                for (def opMap : conditions) {
                    String opKey = opMap.key
                    QOperator op = mapping[opKey.toUpperCase()]
                    for (def entry : opMap.value) {
                        ON (e.COLUMN(entry.key), op, PARAM("where." + opKey + "." + entry.key))
                    }
                }

            }
        }
    }
}