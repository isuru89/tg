
def data = $SESSION.data
def only = $SESSION._only ?: []

$DSL.insert {

    TARGET(TABLE($SESSION.entity).alias("e"))

    for (def entry : data) {
        if (only.contains(entry.key)) {
            CELL_DATA (entry.key, PARAM("data." + entry.key))
        }
    }

    RETURN_KEYS()
}