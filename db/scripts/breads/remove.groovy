
$DSL.delete {

    TARGET(TABLE($SESSION.entity).alias("e"))

    WHERE {
        EQ (e.COLUMN($SESSION.entity + "_id"), PARAM("id"))
    }
}