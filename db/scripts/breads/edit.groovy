
def data = $SESSION.data as Map
def only = $SESSION._only ?: []

$DSL.update {

    TARGET(TABLE($SESSION.entity).alias("e"))

    WHERE {
        EQ (e.COLUMN($SESSION.entity + "_id"), PARAM("id"))

        // check whether user has permission to access.
        if (data.containsKey('_usr')) {
            AND
            EQ (e.COLUMN(data._usr.against), PARAM("data._usr.id"))
        }
    }

    SET {
        for (def entry : data) {
            String col = entry.key
            
            if (only.contains(col)) {
                EQ (e.COLUMN(col), PARAM("data." + col))
            }
        }
    }
}