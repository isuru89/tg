var express         = require('express');
var bodyParser      = require('body-parser');
var ExpressSession  = require('express-session');
var authMW          = require('./auth');
var utils           = require('./utils');
var winston         = require('winston');

// setup loggers
winston.add(winston.transports.File, { name: 'warn-file', filename: 'logs.log', level: 'warn' });
winston.handleExceptions(new winston.transports.File({ name: 'unhandled-ex', filename: 'exceptions.log' }))

var profile = process.env.NODE_ENV || 'dev';
if (profile) {
    winston.info(`Activated profile: ${profile}`);
}
var config          = require('config');
var app             = express();
var cookieParser    = require('cookie-parser')(config.sessions.secret);
var connectRedis    = require('connect-redis');
var RedisStore      = connectRedis(ExpressSession);
var rClient         = require('redis').createClient(config.redis || {});
var sessionStore    = new RedisStore({client: rClient});

var session = ExpressSession({
  store: sessionStore,
  secret: config.sessions.secret,
  resave: true,
  saveUninitialized: true,
  key: 'jsessionid'
});

app.set('x-powered-by', false); // disable x-powered-by tag

// body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// allow cross-orgin headers
app.use(utils.corsMiddleware);

winston.info('registering analytics middleware...');
var analytics = require('./analytics');
analytics.middleware(app);
analytics.responseTime(app);

// serve static files
winston.info('serving static files...');
app.use('/static', express.static('public'));
app.use('/public', require('./routers/public'));

app.use(cookieParser);
app.use(session);

// mount api routes all having proper authentication
var mainRouter = require('./routers')(app);
app.use('/tg', mainRouter);

// mount auth router to retrieve token
winston.info('mounting auth routes...');
app.use('/auth', authMW.router);

if (process.env.NODE_ENV != 'production') {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED=0;
    winston.warn('Caution!!! Enabling database clean operation!')
    app.use('/_/clean', require('./routers/danger'));
}

var server = require('http').Server(app);

server.listen(config.port, () => {
    winston.info(`TaskGraph server is up and running in port ${config.port}...`);
});

// create dirs if not exist...
utils.createPublicNXDirs();

// set session/cookie parsers
app.sessionStore = sessionStore;
app.cookieParser = cookieParser;
app.session = session;
require('./events').init(app);

module.exports = app;