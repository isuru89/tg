var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');

router.get('/view', (req, res) => {
    const tempToken = req.query.token || '';
    const taskId = req.query.task || -1;
    const userId = req.query.user || -1;
    
    nyserver.execute('tasks/public/viewTask', { user_id: userId, token: tempToken, task_id: taskId })
        .then(json => {
            if (json.result.length > 0) {
                res.json({});
                // @TODO render page for viewing task
            } else {
                res.status(401).json({ error: 'You are not authorized to view this task' });
            }
        });
});

router.post('/edit', (req, res) => {
    const tempToken = req.query.token || '';
    const taskId = req.query.task || -1;
    const userId = req.query.user || -1;

    utils.validateToken(userId, token, taskId, (err, result) => {
        if (!err) {
            res.redirect('/tg/tasks/edit');
        } else {
            res.status(401).json({ error: err });
        }
    });
});

module.exports = router;