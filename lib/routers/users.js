const onlyedit  = ['first_name', 'last_name', 'email', 'profile_image']

var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');
var async       = require('async');
const _         = require('lodash');

router.post('/edit', _userMW, (req, res) => {
    var body = req.body;

    nyserver.executeSend('breads/edit', 
        { entity: 'user', id: body.id, data: body.data, _only: onlyedit }, 
        res, 
        utils.updateFilter);

    if (req.body.data.profile_image) {
        // profile image is going to be changed...
        utils.writeImageFile(req.body.data.profile_image, '/public/imgs/users/' + req.user.id + '.jpg');
    }
});

router.post('/read', (req, res) => {
    nyserver.executeSend('users/getUserById', { user_id: req.body.id || req.user.id }, 
            res, json => {
                if (req.body.id == req.user.id) {
                    return json;
                } else {
                    // remove mobile no
                    delete json[0].mobile_no;
                    delete json[0].email;
                    return json;
                }
        });
});

router.post('/lookup', (req, res) => {
    var phoneNo = req.body.phoneNo;
    if (!_.isArray(phoneNo)) {
        phoneNo = [phoneNo];
    }

    const phoneNos = _.map(phoneNo, function (p) { return { phone_no: p } });
    nyserver.executeSend('users/getUsersByPhones', { __batch__: phoneNos }, res);
});

function _userMW(req, res, next) {
    if (req.user.id != req.body.id) {
        res.status(403).send({ error: "You can't access information of other users!" });
    } else {
        next();
    }
}

module.exports = router;