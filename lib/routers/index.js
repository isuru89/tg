var express         = require('express');
var auth            = require('../auth');

module.exports = function (app) {

    var router = express.Router();

    // authenticated middleware
    router.use(auth.middleware);
    
    router.use('/tasks', require('./tasks'));
    router.use('/users', require('./users'));
    router.use('/comments', require('./comments'));
    router.use('/lists', require('./lists'));
    
    return router;
}