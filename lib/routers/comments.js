const onlyadd   = ['task', 'message', 'posted_at', 'by_user']
var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');
var events      = require('../events');

// add a new comment
router.post('/add', (req, res) => {
    const curTime = new Date().getTime();
    var _data = req.body.data || {};
    _data.by_user = req.user.id;
    _data.posted_at = _data.posted_at || curTime;
    const taskId = _data.task;

    nyserver.execute('comments/addComment', 
                { entity: 'comment', data: _data, _only: onlyadd, 
                    user_id: req.user.id, 
                    task_id: taskId,
                    last_read_time: curTime })
        .then(json => {
                res.json(utils.updateFilter(json.result));

                // @TODO notify users about the new comment
                events.publishToTaskUsers(events.types.COMMENT_ADD, taskId, { taskId: taskId, by: _data.by_user }, _data.by_user);
                
        }).catch(err => {
            res.status(500).send({ error: err });
        })
});

router.post('/read', (req, res) => {
    nyserver.executeSend('comments/readComment', { id: req.body.id }, res);
});

// get all unread comment count for given task
router.post('/unreadCount', (req, res) => {
    if (req.body.task_id) {
        nyserver.execute('comments/getUnreadCount', { user_id: req.user.id, task_id: req.body.task_id })
            .then(json => {
                res.json(json.result);
            });
    } else {
        res.status(400).send({ error: 'Parameter task_id is missing!' });
    }
});

// get all unread comment count for all tasks
router.post('/unreadCounts', (req, res) => {
    nyserver.executeSend('comments/getUnreadCountByTask', { user_id: req.user.id }, res);
});

// update user last read time to given or current time.
router.post('/updateLastReadTime', (req, res) => {
    var ms = req.body.time || new Date().getTime();
    if (req.body.task_id) {
        nyserver.executeSend('comments/updateLastReadTime', 
            {   user_id:        req.user.id, 
                task_id:        req.body.task_id,
                last_read_time: ms 
            }, res);

    } else {
        res.status(400).send({ error: 'Parameter task_id is missing!' });
    }
});

module.exports = router;