var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');

router.post('/db', (req, res) => {
    nyserver.executeSend('zero/truncateTables', { tables: req.body.tables || [] }, res);
});

router.post('/activationKey', (req, res) => {
    nyserver.executeSend('zero/getKey', { phone_no: req.body.phoneNo, device_id: req.body.deviceId }, res);
});

module.exports = router;