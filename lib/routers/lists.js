const onlyedit  = ['title', 'small_image'];

var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');
const _         = require('lodash');
const async     = require('async');
const events    = require('../events');
var analytics   = require('../analytics');
var logger      = require('winston');

// get all my boards (mine and shared with me)
router.get('/my', (req, res) => {
    const nydata =  { user_id: req.user.id };

    nyserver.executeSend('lists/myLists', nydata, res);
});

router.post('/share', (req, res) => {
    var body = req.body;
    var uids = _.isArray(body.user_id) ? body.user_id : [body.user_id];
    const lId = body.list_id;
    var allUsers = [];

    async.map(uids, utils.createTempUserAsync, (err, userList) => {
        allUsers = userList;

        async.series([
            function (callback) {
                nyserver.execute('lists/shareList', { list_id: lId, shared_status: true, __batch__: userList })
                    .then(shareJson => callback(null, shareJson))
                    .catch(err => { if (err) callback(err); });
            },
            function (callback) {
                // @TODO notify shared users about list
                events.publishToListUsers(events.types.LIST_SHARE, lId, { listId: lId, by: req.user.id }, req.user.id);
                callback(null, true);
            }
        ], function (err, results) {
            if (!err) {
                res.json(utils.batchFilter(results[0].result));
                analytics.userListShared(req.user.id, allUsers);
            } else {
                res.status(500).json({ error: err });
                logger.error('Error occurred while sharing list! ' + JSON.stringify(req.body));
                analytics.reportError('Share List', err);
            }
        });
        
    });
});

// get all users who has been shared to this given board
router.post('/sharedUsers', (req, res) => {
    if (req.body.list_id) {
        nyserver.executeSend('lists/sharedUserList', { list_id: req.body.list_id, user_id: req.user.id }, res);
    } else {
        res.status(400).json({ error: 'Mandatory list_id parameter is missing!' });
    }
});

// remove a user from the given board
router.post('/removeSharedUser', (req, res) => {
    const data = req.body;
    if (data.list_id && data.user_id) {
        nyserver.executeSend('lists/removeSharedUser', 
            { list_id: data.list_id, user_id: req.user.id, remove_user: data.user_id }, 
            res,
            utils.updateFilter);
            
        analytics.userListShareRemoved(req.user_id);

    } else {
        res.status(400).type('json').send({ error: 'List id or user id is missing!' });
    }
})

// get all top level tasks of the board
router.post('/tasks', (req, res) => {
    var status = req.body.status || [0, 1];
    status = _.isArray(status) ? status : [status];

    if (req.body.list_id) {
        nyserver.executeSend('tasks/listTasks', { list_id: req.body.list_id, user_id: req.user.id, status: status }, res);
    } else {
        res.status(400).type('json').send({ error: 'Mandatory list_id parameter is missing!' });
    }
});

router.post('/remove', (req, res) => {
    nyserver.executeSend('lists/removeList', { list_id: req.body.id, user_id: req.user.id }, res, utils.updateFilter);
});

router.post('/add', (req, res) => {
    const time = req.body.time || new Date().getTime();
    nyserver.executeSend('lists/addList', { title: req.body.title, time: time, user_id: req.user.id }, res, utils.updateFilter)
        .then(data => analytics.userListCreated(req.user.id, req))
        .catch(err => {
            logger.error('Error occurred while adding new list! ' + JSON.stringify(req.body));
            analytics.reportError('List Add', err);
        });
});

router.post('/edit', (req, res) => {
    var _data = req.body.data || {};
    _data._usr = { id: req.user.id, against: 'owner' };

    nyserver.execute('breads/edit', { entity: 'list', id: req.body.id, data: _data, _only: onlyedit })
        .then(editJson => {
            const ures = utils.updateFilter(editJson.result);
            res.json(ures);

            if (ures.success && _data.small_image) {
                // list image is going to be changed...
                utils.writeImageFile(_data.small_image, '/public/imgs/lists/' + req.body.id + '.jpg');
            }
        }).catch(err => {
            logger.error('Error occurred while editing list! ' + JSON.stringify(req.body));
            analytics.reportError('List Edit', err);
        }); 
});

module.exports = router;