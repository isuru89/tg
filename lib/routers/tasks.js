const onlyadd   = ['title', 'description', 'due_start', 'due_end', 'assigned_to', 'created_by', 'created_at'];
const onlyedit  = ['title', 'description', 'due_start', 'due_end', 'status', 'assigned_to'];

var router      = require('express').Router();
var nyserver    = require('../nyserver');
var utils       = require('../utils');
var _           = require('lodash');
var async       = require('async');
var events      = require('../events');
var services    = require('../services');
var analytics   = require('../analytics');
var logger      = require('winston');

router.post('/feed', (req, res) => {
    var nydata = {
        base_time:  req.body.time || new Date().getTime(),
        direction:  req.body.direction || 0,
        offset:     req.body.offset || 0,
        limit:      req.body.limit || 20,
        user_id:    req.user.id,
        status:     req.body.status || [0]
    }

    nyserver.executeSend('tasks/myTasks', nydata, res);
});

router.post('/add', (req, res) => {
    var tId = -1;
    var lId = req.body.list_id || req.user.defBoard;
    var pId = req.body.parent_task || -1;
    var _data = req.body.data || {};
    _data.created_at = _data.created_at || new Date().getTime();
    _data.created_by = req.user.id;
    // if there is no assignee then it will be assigned to me
    _data.assigned_to = _data.assigned_to || (-1 * req.user.id);   

    utils.createTempUserAsync(_data.assigned_to, function (err, theUser) {
        if (err) {
            res.status(500).json({ error: err });
            logger.error('Error occurred while mapping user ' + _data.assigned_to + '!' + JSON.stringify(err));
            return;
        }

        _data.assigned_to = theUser.user_id;
        const isTemp = theUser.temp;
        var nydata = {  entity: 'task', 
                        data: _data, 
                        list_id: lId, 
                        parent_task: pId, 
                        user_id: req.user.id, 
                        _only: onlyadd 
                    };

        nyserver.execute('tasks/addTask', nydata)
        .then(json => {
            const keys = json.result[1].keys;
            if (keys && keys.length > 0) {
                tId = keys[0];
                res.json({ success: true, keys: [tId] });
                analytics.userTaskCreated(req.user.id, _data.assigned_to, req);

                if (req.user.id != _data.assigned_to) {
                    events.publish(events.types.ASSIGNMENT, _data.assigned_to, { taskId: tId, listId: lId });
                    
                    // send a sms message to the temp user.
                    if (isTemp) {
                        services.sendTaskViewPage(tId, theUser.user_id);
                        analytics.userInvited(theUser.user_id);
                    }
                }
                events.publishToListUsers(events.types.TASK_ADD, lId, { taskId: tId, listId: lId }, req.user.id);
                
            } else {
                logger.error('Failed to add task! ' + JSON.stringify(req.body));
                res.json({ success: false, keys: []});
            }

        }).catch(err => {
            res.status(500).json({ error: err });
            logger.error('Failed to add task! ' + JSON.stringify(err));
        });
    });
    
});

router.post('/edit', (req, res) => {
    var _data = req.body.data || {};
    const tId = req.body.id;
    const inList = req.body.list_id;

    async.series([
        function (callback) {
            utils.createTempUserAsync(_data.assigned_to, function (err, theUser) {
                if (!err) {
                    _data.assigned_to = theUser.user_id;
                    nyserver.execute('tasks/editTask', { id: tId, user_id: req.user.id, list_id: inList, data: _data, _only: onlyedit })
                        .then(editJson => {
                            callback(null, editJson);

                            // send a sms to assigned user.
                            if (theUser.temp) {
                                services.sendTaskViewPage(tId, theUser.user_id);
                                analytics.userInvited(theUser.user_id);
                            }
                        })
                        .catch(err => { 
                            if (err) callback(err); 
                        });
                } else {
                    callback(err, null);
                }
            })
        },
        function (callback) {
            if (_data.assigned_to) {
                events.publish(events.types.ASSIGNMENT, _data.assigned_to, { taskId: tId });
                analytics.userTaskAssigned(req.user.id, _data.assigned_to, req);
            }
            if (_data.hasOwnProperty('status')) {
                events.publishToTaskUsers(events.types.STATUS_CHANGE, tId, { taskId: tId, newStatus: _data.status }, req.user.id);
            }

            // send notification to all users in every list this task belongs to
            nyserver.execute('tasks/findBelongLists', { task_id: tId })
                .then(listJson => {
                    listJson.result.forEach(function (lstId) {
                        events.publishToListUsers(events.types.TASK_EDIT, lstId, { taskId: tId }, req.user.id);
                    });
                });
                
            callback(null, true);
        }
    ], function (err, results) {
        if (!err) {
            res.json(utils.updateFilter(results[0].result));
        } else {
            res.status(500).json({ error: err });
            logger.error('Error occurred while editing task! ' + JSON.stringify(err));
        }
    });
    
});

router.post('/read', (req, res) => {
    nyserver.executeSend('tasks/readFullTask', { task_id: req.body.id }, res);
});

router.post('/remove', (req, res) => {
    nyserver.executeSend('tasks/removeTask', 
        { task_id: req.body.id, user_id: req.user.id }, 
        res,
        utils.updateFilter);
});

router.post('/move', (req, res) => {
    var data = req.body || {};
    nyserver.execute('tasks/moveTask', { task_id: data.task_id, from_list: data.from_list, to_list: data.to_list })
        .then(json => {
            if (json.result[0].count > 0) {
                res.json(utils.updateFilter(json.result));
            } else {
                logger.error('Failed to move task! ' + JSON.stringify(req.body));
                return Promise.reject(new Error('Failed to move task!'));
            }

        }).catch(err => {
            res.status(500).send({ error: err.message });
            logger.error('Failed to move task! ' + JSON.stringify(err));
        });
});

router.post('/getComments', (req, res) => {
    const body = req.body;
    nyserver.executeSend('tasks/getCommentsOfTask', 
        { task_id: body.task_id, 
            limit: body.limit || 10,
           offset: body.offset || 0
         }, res);
});

router.post('/share', (req, res) => {
    var body = req.body;
    const ms = new Date().getTime();
    const uids = _.isArray(body.user_id) ? body.user_id : [body.user_id];
    const uidArray = uids;
    var sUsers = [];

    async.map(uids, utils.createTempUserAsync, (err, userList) => {
        sUsers = userList;

        async.series([
            function (callback) {
                nyserver.execute('tasks/shareTask', { task_id: body.task_id, all_users: uidArray, __batch__: userList, current_time: ms })
                    .then(shareJson => callback(null, shareJson))
                    .catch(err => { if (err) callback(err); });
            },
            function (callback) {
                events.publishToTaskUsers(events.types.TASK_SHARE, body.task_id, { taskId: body.task_id, by: req.user.id });
                callback(null, true);

                // send sms about sharing...
                _.forEach(userList, aUser => {
                    if (aUser.temp) services.sendTaskViewPage(body.task_id, aUser.user_id, 'TaskGraph: A task has been shared with you. ');
                });
            }

        ], function (err, results) {
            if (!err) {
                res.json(utils.batchFilter(results[0].result));
                analytics.userTaskShared(req.user.id, sUsers, req);
            } else {
                res.status(500).json({ error: err });
            }
        });
        
    });
});

module.exports = router;