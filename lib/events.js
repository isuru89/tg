const configs   = require('config');
const _         = require('lodash');
const async     = require('async');
const nyserver  = require('./nyserver');
var logger      = require('winston');
var jwt         = require('jsonwebtoken');
var Pusher      = require('pusher');
var pusher;
var enabled = configs.alerts.pusher.enabled;

const errConnect = err => logger.error(err);

module.exports.init = function (app) {
    if (enabled) {
        logger.info('Pusher Notifications enabled.');

        const pusherConfig = configs.alerts.pusher;
        pusher = new Pusher({
            appId: pusherConfig.appId,
            key: pusherConfig.pusherKey,
            secret: pusherConfig.pusherSecret,
            authEndPoint: '/pusher/auth',
            encrypted: true
        });

        app.post('/pusher/auth', _pusherAuth);
    } else {
        logger.warn('Pusher Notifications disabled.')
    }
}

function _pusherAuth(req, res) {
    var body = req.body;
    var socketId = body.socket_id;
    var channel = body.channel_name;
    const authKey = req.headers['x-access-token'];

    if (authKey) {
        jwt.verify(authKey, configs.appSecret, (err, user) => {
            if (!err && channel.endsWith('-user-' + user.id)) {
                var auth = pusher.authenticate(socketId, channel);
                res.send(auth);
            } else {
                res.status(403).send();
            }
        });
        
    } else {
        res.status(403).send();
    }
}

module.exports.publish = _broadcastToUsers;

module.exports.publishToTaskUsers = function (eventType, taskId, data, except) {
    if (enabled) {
        nyserver.execute('tasks/getAllCollaborators', { task_id: taskId })
            .then(json => {
                var array = _.map(json.result, item => item.user);
                if (array.length > 0) {
                    _broadcastToUsers(eventType, _.filter(array, function (u) { return u != except; }), data);
                }
            });
    }
}

module.exports.publishToListUsers = function (eventType, listId, data, except) {
    if (enabled) {
        nyserver.execute('lists/getSharedUserIds', { list_id: listId })
            .then(json => {
                var array = _.map(json.result, item => item.user);
                if (array.length > 0) {
                    _broadcastToUsers(eventType, _.filter(array, function (u) { return u != except; }), data);
                }
            });
    }
}

module.exports.types = {
    TASK_ADD: 'Task-Add',
    TASK_EDIT: 'Task-Edit',
    STATUS_CHANGE: 'Status-Change',
    ASSIGNMENT: 'Assignment',
    TASK_SHARE: 'Task-Share',

    COMMENT_ADD: 'Comment-Add',

    LIST_SHARE: 'List-Share'
}

function _broadcastToUsers(eventType, targetUsers, data) {
    if (enabled) {
        if (_.isArray(targetUsers)) {
            _.forEach(targetUsers, function (uid) {
                pusher.trigger('private-user-' + uid, eventType || 'main', data);
            });
        } else {
            pusher.trigger('private-user-' + targetUsers, eventType || 'main', data);
        }
    }
}