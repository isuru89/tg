var configs     = require('config');
var twilio      = require('twilio')(configs.alerts.sms.accountSID, configs.alerts.sms.authToken);
var logger      = require('winston');
var analytics   = require('./analytics');

module.exports.sendSMS = (to, text) => {
    return new Promise(function (resolve, reject) {
        twilio.sendMessage({
            body: text,
            to: to,
            from: configs.alerts.sms.sendingNumber
        }, function (err, data) {
            if (err) {
                logger.error('Failed to send sms to ' + to + '! ' + JSON.stringify(err));
                reject(err);
                analytics.reportError('sms-send', err);
                
            } else {
                logger.info("Successfully sent msg to " + to + ", message: " + text);
                resolve(data);
            }
        });
    });
}