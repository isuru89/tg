var router      = require('express').Router();
var nyserver    = require('./nyserver');
var alertz      = require('./alerts');
var configs     = require('config');
var jwt         = require('jsonwebtoken');
var utils       = require('./utils');
var analytics   = require('./analytics');
var logger      = require('winston');

module.exports = {
    middleware: _authMW2,
    router: router
}

router.post('/login', (req, res) => {
    const authKey = req.headers['x-access-token'];
    if (authKey) {
        jwt.verify(authKey, configs.appSecret, (err, user) => {
            if (!err) {
                req.session.user = { id: user.id, defBoard: user.defBoard };
                res.json({ success: true, message: 'Use this session for subsequent calls.' });

                analytics.userLoggedIn(user.id, req);

            } else {
                req.session.destroy(err => res.status(401).json({ error: 'Invalid access token!' }));
            }
        });
    } else {
        res.status(403).send({ error: 'You are not allow to login without user token!' });
    }
});

router.post('/register', (req, res) => {
    const phone = req.body.phoneNo;
    const deviceId = req.body.deviceId;
    const time = new Date().getTime();

    if (phone && deviceId) {
        nyserver.execute('users/findOrCreateUser', { user_phone: phone, device_id: deviceId, current_time: time })
            .then(json => {
                const result = json.result;
                if (result.temporary) {
                    const pno = result.user.mobile_no;
                    const actCode = result.activationCode;

                    analytics.userRegistered(phone, deviceId);
            
                    alertz.sendSMS(pno, 'Your ' + configs.appName + ' activation code is ' + actCode)
                        .then(smsResponse => { res.json({ success: true,
                                message: 'User successfully registered! Your activation code will recieve to you shortly!' });
                                analytics.verificationCodeSendSuccess(phone);
                        }).catch(smsErr => { res.json({ success: false,
                                message: `User successfully registered! But ${configs.appName} unable to send the verification code! Try again registration later!` });
                                analytics.verificationCodeSendFailed(phone, deviceId);
                        });

                    
                } else {
                    var errJson = { error: 'There is already a user exist with given phone number!' };
                    if (result.resetToken) {
                        errJson.resetToken = result.resetToken;
                    }
                    res.status(401).json(errJson);
                }

            }).catch(err => {
                res.status(500).json({ error: 'Failed to find a user!' });
            });

    } else {
        res.status(400).json({ error: 'No valid phone number or device is provided for registration!' });
    }
});

router.get('/activate', (req, res) => {
    const code = req.query.code;
    const phone = req.query.phoneNo;
    const deviceId = req.query.deviceId;
    const time = new Date().getTime();

    _activateRouter(code, phone, deviceId, time, req, res);
});

router.post('/activate', (req, res) => {
    const code = req.body.code;
    const phone = req.body.phoneNo;
    const deviceId = req.body.deviceId;
    const time = new Date().getTime();

    _activateRouter(code, phone, deviceId, time, req, res);
});

router.post('/reset', (req, res) => {
    const phone = req.body.phoneNo;
    const deviceId = req.body.deviceId;
    const resetToken = req.body.token;
    const time = new Date().getTime();

    if (phone && deviceId && resetToken) {
        nyserver.execute('users/resetUserDevice', { user_phone: phone, device_id: deviceId, current_time: time, reset_token: resetToken })
            .then(resetJson => {
                var result = utils.updateFilter(resetJson.result);
                if (result.success) {
                    res.json({ success: true, message: 'Call register to get the new token for the device.' });
                } else {
                    res.status(401).json({ error: 'You are not authorized to perform the key reset!' });
                }
            });

    } else {
        res.status(400).json({ error: 'No valid phone or device or token is provided for reset!' });
    }
});

function _authMW2(req, res, next) {
    if (req.user) {
        delete req.user;
    }

    if (req.session && req.session.user && req.session.user.id > 0) {
        req.user = req.session.user;
        next();
    } else {
        res.status(403).send({ error: 'You are not authorized to access the api!' });
    }
}

function _createToken(user, defaultBoard, cb) {
    var tuser = {   id:         user.user_id,
                    phone_no:   user.mobile_no,
                    //device:     user.device_id,
                    defBoard:   defaultBoard,
                    issuedTime: new Date().getTime()
                };
    return jwt.sign(tuser, configs.appSecret, { algorithm: 'HS256' }, function (err, token) {
        if (err) {
            return Promise.reject(err);
        } else {
            return nyserver.execute('users/updateToken', { user_id: user.user_id, device_id: user.device_id, token: token })
                        .then(json => cb(token));
        }
    });
}

function _activateRouter(code, phone, deviceId, time, req, res) {
    if (!code || !phone || !deviceId) {
        res.status(400).json({ error: 'Required parameters missing for activation!' });
        return;
    }

    nyserver.execute('users/getUserByPhoneDevice', { phone_no: phone, device_id: deviceId })
        .then(json => {
            var result = json.result;
            if (result && result.length == 1) {
                var user = result[0];
                if (code != user.activation_code) {
                    return Promise.reject(new Error('The activation code is incorrect!'));
                } else {
                    return Promise.resolve(user);
                }
            } else {
                return Promise.reject(new Error(`Unknown phone number or device ${phone}!`));
            }

        }).then(user => {
            return nyserver.execute('users/activateUser', { phone_no: phone, device_id: deviceId, activated_time: time })
                .then(actRes => {
                        const success = actRes.result.success;
                        if (success) {
                            _createToken(user, actRes.result.defaultBoard,
                                token => res.json({ success: true, message: 'Activation success!', token: token, user_id: user.user_id }));
                            analytics.userActivated(phone, deviceId, req);

                        } else {
                            return Promise.reject(new Error('Activation failed. Please try again after sometime!'));
                        }
                });

        }).catch(err => {
            logger.error('Error occurred while activating... ' + JSON.stringify(err));
            res.status(403).send({ error: err.message });
        });
}