const fetch     = require('node-fetch');
const configs   = require('config');
const alertz    = require('./alerts');
const nyserver  = require('./nyserver');
const uuidV4    = require('uuid/v4');
const analytics = require('./analytics');

const GOOGLE_URL_SHORT_API = "https://www.googleapis.com/urlshortener/v1/url";
const API_KEY = configs.services.urlShortnerApiKey;

module.exports.sendTaskViewPage = function (taskId, user, msg) {
    const msgText = msg || 'TaskGraph: A task has been assigned to you. ';
    const token = uuidV4().replace(/-/g, '');
    var future = new Date(2099, 1, 22).getTime();
    nyserver.execute('tasks/createTempToken', { task_id: taskId, user_id: user, token: token, expire_when: future })
        .then(userJson => {
            const taskUrl = configs.host + "/public/view?task=" + taskId + "&user=" + user + "&token=" + token;
            _getShortenedUrl(taskUrl, shortUrl => {
                alertz.sendSMS(userJson.result[0].phone_no, msgText + shortUrl.id);
            });
        });
}

module.exports.getShortenedUrl = _getShortenedUrl;

function _getShortenedUrl(url, cb) {
    fetch(GOOGLE_URL_SHORT_API + '?key=' + API_KEY, {
        method:     'POST',
        body:       JSON.stringify({ longUrl: url }),
        headers:    { 
            'Content-Type': 'application/json'
         }
    })
    .then(res => res.json())
    .then(json => {
        if (json.error) {
            analytics.reportError('url-short', { message: json.error });
            return cb({id: url});
        } else {
            return cb(json);
        }
    })
    .catch(err => {
        analytics.reportError('url-short', err);
        return cb({id: url});
    });
}