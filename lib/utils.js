const _     = require('lodash');
const async = require('async');
const mkdirp = require('mkdirp');
const nyserver = require('./nyserver');
var fs = require('fs');
var logger = require('winston');

module.exports.corsMiddleware = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8100");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token, cookie, set-cookie");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

  // accept cors preflight request...
  if (req.method == 'OPTIONS') {
    res.status(200).send();
  } else {
    next();
  }
}

module.exports.validateToken = function (user, token, task, cb) {
    nyserver.execute('tasks/readTempToken', { task_id: task, user_id: user, token: token })
        .then(readJson => {
            if (readJson.result.length > 0) {
                cb(null, readJson.result[0]);
            } else {
                cb(new Error('Not authorized to change task attributes!'), null);
            }
        });
}

module.exports.createTempUserAsync = function(uid, callback) {
    if (uid && (isNaN(uid) || typeof uid === 'string')) {
        nyserver.execute('users/inviteUser', { user_phone: uid })
            .then(json => callback(null, { user_id: json.result.userId, temp: json.result.isTemp }))
            .catch(err => { if (err) callback(err); })
    } else {
        callback(null, { user_id: uid, temp: false });
    }
}

/**
 * Converts result returned by NyQL when insert/update/delete 
 * command is executed.
 */
module.exports.updateFilter = function (jsonData) {
    var item = jsonData[0];
    var theKeys = [];
    if (jsonData.length > 1) {
        theKeys = jsonData[1].keys;
    }
    return {
        success:    item.hasOwnProperty('count') && item.count > 0,
        keys:       theKeys
    };
}

/**
 * Converts result returned by NyQL when batch insert/update/delete 
 * command is executed.
 */
module.exports.batchFilter = function (jsonData) {
    const item = jsonData[0];
    return {
        success:    (_.isArray(item.count) ? item.count.length > 0 : item.count > 0),
        keys:       item.keys || []
    };
}

module.exports.writeImageFile = function (imgData64, filepath) {
    var matches = imgData64.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var imgdata;
    if (matches != null && matches.length >= 3) {
        imgdata = new Buffer(matches[2], 'base64');
    } else {
        imgdata = new Buffer(imgData64, 'base64');
    }
    fs.writeFile(process.cwd() + filepath, imgdata, function(err) { 
        if (err) {
            logger.error("Error writing user image file: " + req.user.id + "! [" + err);
        }
    });
}

// create static dirs if not exist
module.exports.createPublicNXDirs = function () {
    const publicDirs = [process.cwd() + '/public/imgs/lists', process.cwd() + '/public/imgs/users'];
    async.eachSeries(publicDirs, function (dirpath, callback) {
            mkdirp(dirpath, err => {
                if (err) {
                    callback(err);
                } else {
                    callback();
                }
            });

        }, function (err) {
            if (err) {
                logger.error("Error occurred while creating public static directories! " + err);
            }
        });
}