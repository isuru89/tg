var fetch       = require('node-fetch');
var configs     = require('config');
var logger      = require('winston');

function _callNyServer(scriptName, data) {
    var nyData = {
        scriptId: scriptName,
        data: data
    };

    return fetch(configs.ny.url + '/execute', {
        method:     'POST',
        body:       JSON.stringify(nyData),
        headers:    { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + configs.ny.token
         }
    }).then(res => res.json());
}

module.exports.execute = _callNyServer;

module.exports.executeSend = function (scriptName, data, response, filterResult) {
    return _callNyServer(scriptName, data)
        .then(nyres   => {
            const transformed = filterResult ? filterResult(nyres.result) : nyres.result;
            response.json(transformed);
            return Promise.resolve({});

        }).catch(err    => {
            logger.error('Error occurred while executing nyserver script ' + scriptName + '! ' + err);
            response.status(500).send({ error: err });
            return Promise.reject(err);
        });
}