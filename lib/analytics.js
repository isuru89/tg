var fetch       = require('node-fetch');
var configs     = require('config');
var _           = require('lodash');
var URL         = configs.services.analyticsKey;
const enabled   = configs.services.analyticsEnabled;
var logger      = require('winston');
var ua          = require('universal-analytics');
var visitor     = ua(configs.services.analyticsKey);

if (enabled) {
    logger.info('Analytics enabled in ' + URL + '...');
} else {
    logger.warn('Analytics disabled.');
}

module.exports.middleware = function (app) {
    if (enabled) {
        app.use(function (req, res, next) {
            next();
            visitor.event('API Call IP', req.ip ? req.ip+'' : 'Unknown', req.path).send();
            visitor.event('API Call Agent', _reqAgent(req), req.path).send();
        });
    }
}

module.exports.responseTime = function (app) {
    if (enabled) {
        var rtime = require('response-time');

        app.use(rtime(function (req, res, time) {
            visitor.timing('API Response Time', req.path, parseInt(time, 10)).send();
        }));
    }
}

module.exports.userLoggedIn = function (user, req) {
    if (enabled) {
        visitor.event('User', 'Login', user+'').send();
    }
}

module.exports.userRegistered = function (userPhone, device, req) {
    if (enabled) {
        visitor.event('User', 'Register', userPhone).send();
    }
}

module.exports.userActivated = function (userPhone, device, req) {
    if (enabled) {
        visitor.event('User', 'Activate', userPhone).send();
    }
}

module.exports.userInvited = function (user) {
    if (enabled) {
        visitor.event('User', 'Invitation', user+'').send();
    }
} 

module.exports.userSocketOpened = function (user) {

}

module.exports.userSocketClosed = function (user) {

}

module.exports.verificationCodeSendSuccess = function (user) {
    if (enabled) {
        visitor.event('Notifications', 'Verification SMS', ''+user).send();
    }
}

module.exports.verificationCodeSendFailed = function (user, device) {
    if (enabled) {
        visitor.exception('Verification SMS Send Failure').send();
    }
}

module.exports.userTaskCreated = function (user, toUser, req) {
    if (enabled) {
        visitor.event('Content', 'Task Add', 'By-'+user).send();
        if (toUser && toUser != user) {
            visitor.event('Content', 'Task Assign', 'By-'+user).send();
        }
    }
}

module.exports.userTaskAssigned = function (user, toUser, req) {
    if (enabled) {
        visitor.event('Content', 'Task Assign', 'By-'+user).send();
    }
}

module.exports.userTaskShared = function (user, toUsers, req) {
    if (enabled) {
        if (_.isArray(toUsers)) {
            visitor.event('Content', 'List Share', 'By-'+user, toUsers.length).send();
        } else {
            visitor.event('Content', 'List Share', 'By-'+user, 1).send();
        }
    }
}

module.exports.userListCreated = function (user, req) {
    if (enabled) {
        visitor.event('Content', 'List Add', 'By-'+user).send();
    }
}

module.exports.userListShared = function (user, toUsers, req) {
    if (enabled) {
        if (_.isArray(toUsers)) {
            visitor.event('Content', 'List Share', 'By-'+user, toUsers.length).send();
        } else {
            visitor.event('Content', 'List Share', 'By-'+user, 1).send();
        }
    }
}

module.exports.userListShareRemoved = function (user) {
    if (enabled) {
        visitor.event('Content', 'List Share Removal', 'By-'+user).send();
    }
}

module.exports.reportError = function (errType, err) {
    if (enabled) {
        visitor.exception('Error ' + errType).send();
    }
}

function _reqAgent(req) {
    if (req && req.headers) {
        return req.get('User-Agent');
    } else {
        return 'Unknown';
    }
}

function _request(req) {
    if (req && req.headers) {
        return {
            Host: req.get('Host'),
            Referer: req.get('Referer'),
            User_Agent: req.get('User-Agent'),
            Origin: req.get('Origin'),
            Real_IP: req.get('X-Real-IP'),
            X_Forwarded_For: req.get('X-Forwarded-For'),
            X_Forwarded_Host: req.get('X-Forwarded-Host'),
            X_Forwarded_Proto: req.get('X-Forwarded-Proto')
        }
    } else {
        return {};
    }
}