var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
var _           = require('lodash');
var async       = require('async');

const BASE_URL  = utils.BASE_URL;
const _ME       = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.THIRD_USER;

chai.use(chaiHttp);

describe('Sub Task Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;

    var socketBob, socketAlice, socketMe;
    var myDefList = -1;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        socketMe = utils.openChannel(ME);
                        socketAlice = utils.openChannel(ALICE);
                        socketBob = utils.openChannel(BOB);

                        done();
                    });
    });

    describe('#Creating a sub task', () => {
        
        var myAddedTask = -1;
        var myAddedSubTask = -1;
        var bobDefList = -1;
        var bobNewList = -1;

        describe('I create a new task in default list', (done) => {
            
            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        myDefBoard = res.body[0].list_id;
                        done();
                    });
            });

            it('should be able to create a new task to Alice', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { data: { title: 'Accello review discussion', assigned_to: ALICE.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('my default list should have the added task', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: myDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        done();
                    });
            });
        });

        describe('Alice add another sub task under the added one', done => {
            it('added task should not have children tasks', done => {
                utils.doPost(chai, '/tg/tasks/read', ALICE_SESSION, { id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('childCount').eql(0);
                        res.body[0].should.have.property('children');
                        res.body[0].children.should.be.instanceOf(Array).have.lengthOf(0);
                        done();
                    });
            });

            it('Alice should be able to add sub task', done => {
                utils.doPost(chai, '/tg/tasks/add', ALICE_SESSION, { parent_task: myAddedTask, data: { title: 'Sub task by Alice to bob', assigned_to: BOB.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedSubTask = res.body.keys[0];
                        done();
                    });
            });

            it('I should not be able to add a sub task', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { parent_task: myAddedTask, data: { title: 'Sub task by Isuru', assigned_to: BOB.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(false);
                        done();
                    });
            });

            it('added task should have one child task', done => {
                utils.doPost(chai, '/tg/tasks/read', ALICE_SESSION, { id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('childCount').eql(1);
                        res.body[0].should.have.property('children');
                        res.body[0].children.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });
        });

    });


    describe("#Visibility of self sub-tasks", done => {
        var myAddedTask = -1;
        var myAddedSubTask = -1;
        var bobDefList = -1;
        var bobNewList = -1;

        describe('I create a new task in default list', (done) => {
            
            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        myDefList = res.body[0].list_id;
                        done();
                    });
            });

            it('should be able to create a new task assigning to myself', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { data: { assigned_to: ME.user_id, title: 'Sub task visible task' } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('my default list should have the added task', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: myDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array);
                        _.filter(res.body, it => it.task_id === myAddedTask).should.have.lengthOf(1);
                        done();
                    });
            });
        }); 

        describe('I create a sub task under above task', done => {
            it('added task should not have children tasks', done => {
                utils.doPost(chai, '/tg/tasks/read', MY_SESSION, { id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('childCount').eql(0);
                        res.body[0].should.have.property('children');
                        res.body[0].children.should.be.instanceOf(Array).have.lengthOf(0);
                        done();
                    });
            });

            it('should be able to add a sub task', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { parent_task: myAddedTask, data: { assigned_to: ME.user_id, title: 'First Sub task by myself' } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedSubTask = res.body.keys[0];
                        done();
                    });
            });

            it('my default list should have only the parent task', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: myDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array);
                        _.filter(res.body, it => it.task_id === myAddedSubTask).should.have.lengthOf(0);
                        done();
                    });
            });
        })
    });

    after(done => {
        utils.closeChannels([ME, ALICE, BOB], [socketMe, socketAlice, socketBob]);
        done();
    });

});