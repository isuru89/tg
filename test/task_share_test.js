var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
var _           = require('lodash');
var async       = require('async');

const BASE_URL  = utils.BASE_URL;
const _ME       = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.THIRD_USER;

chai.use(chaiHttp);

describe('Task Sharing Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        done();
                    });
    });

    describe('#Task sharing in default list', () => {
        
        var myDefList = -1;
        var myAddedTask = -1;
        var bobDefList = -1;
        var bobNewList = -1;

        describe('I create a new task in default list', (done) => {
            
            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        myDefBoard = res.body[0].list_id;
                        done();
                    });
            });

            it('should be able to create a new task to Alice', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { data: { title: 'Accello review discussion', assigned_to: ALICE.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('my default list should have the added task', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: myDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        done();
                    });
            });

            it('should not have any shared user', done => {
                utils.doPost(chai, '/tg/tasks/read', MY_SESSION, { id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('sharedUsers');
                        res.body[0].sharedUsers.should.be.instanceOf(Array).and.have.lengthOf(0);
                        done();
                    });
            });
        });

        describe('I share the task with Bob', (done) => {
            it('Bob has only default list', done => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        bobDefList = res.body[0].list_id;
                        done();
                    });
            });

            it('I share to the Bob', done => {
                utils.doPost(chai, '/tg/tasks/share', MY_SESSION, { task_id: myAddedTask, user_id: BOB.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('Bob has that task in his default list', done => {
                utils.doPost(chai, '/tg/lists/tasks', BOB_SESSION, { list_id: bobDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        _.filter(res.body, t => t.task_id === myAddedTask).should.not.be.empty;
                        done(); 
                    });
            });

            it('should have Bob as a shared user', done => {
                utils.doPost(chai, '/tg/tasks/read', MY_SESSION, { id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('sharedUsers');
                        res.body[0].sharedUsers.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });
        });

        describe('Bob moves that task to his own list', done => {
            it('Bob creates a new list', done => {
                utils.doPost(chai, '/tg/lists/add', BOB_SESSION, { title: 'bob personal board' })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('keys').lengthOf(1);
                        bobNewList = res.body.keys[0];
                        done();
                    });
            });

            it('Bob should have two lists', done => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        _.filter(res.body, l => l.is_default == true).should.not.be.empty;
                        _.filter(res.body, l => l.list_id === bobNewList).should.not.be.empty;
                        done();
                    });
            });

            it('Bob moves task to the new list', done => {
                utils.doPost(chai, '/tg/tasks/move', BOB_SESSION, { task_id: myAddedTask, from_list: bobDefList, to_list: bobNewList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it("Bob's default list does not contain the task", done => {
                utils.doPost(chai, '/tg/lists/tasks', BOB_SESSION, { list_id: bobDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array);
                        _.filter(res.body, t => t.task_id === myAddedTask).should.be.empty;
                        done(); 
                    });
            });

            it("Bob's new list should contain the task", done => {
                utils.doPost(chai, '/tg/lists/tasks', BOB_SESSION, { list_id: bobNewList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        _.filter(res.body, t => t.task_id === myAddedTask).should.not.be.empty;
                        done(); 
                    });
            });
        });

    });

});