require('./login_test');

var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
const async     = require('async');

const BASE_URL  = utils.BASE_URL;
const _ME      = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.EXTERNAL_USER;

chai.use(chaiHttp);

describe('Basic Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;
    var myBoardId = -1;
    var myAddedTask = -1;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        done();
                    });
    });

    describe('I create a new list', (done) => {
        it('should not be able to add a new list without authentication', (done) => {
            utils.doPost(chai, '/tg/lists/add', null, { title: 'my new board' })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.have.property('error');
                    done();
                });
        });

        it('should be able to create a new list', (done) => {
            utils.doPost(chai, '/tg/lists/add', MY_SESSION, { title: 'my new board' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('keys').lengthOf(1);
                    myBoardId = res.body.keys[0];
                    done();
                });

        });

        it('should be able to edit the title of added list', (done) => {
            utils.doPost(chai, '/tg/lists/edit', MY_SESSION, { id: myBoardId, data: { title: 'my new list title' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });

        it('Alice should not be able to edit my list', (done) => {
            utils.doPost(chai, '/tg/lists/edit', ALICE_SESSION, { id: myBoardId, data: { title: 'my other new list title' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(false);
                    done();
                });
        });
    });

    describe('I create a new task assigning to myself within new list', () => {
        it('should be able to add a task to the list', (done) => {
            utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { list_id: myBoardId, data: { assigned_to: ME.user_id, title: 'my self assigned task' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    myAddedTask = res.body.keys[0];
                    done();
                });
        });

        it('should be able to read the added task', (done) => {
            utils.doPost(chai, '/tg/tasks/read', MY_SESSION, { id: myAddedTask })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    res.body[0].should.have.property('title').eql('my self assigned task');
                    done();
                });
        });

        it('should be able to change title and add description', (done) => {
            utils.doPost(chai, '/tg/tasks/edit', MY_SESSION, { id: myAddedTask, list_id: myBoardId, data: { title: 'Task New Title', description: 'new brief description!' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });

        it('should be able to see it in my time line', done => {
            utils.doPost(chai, '/tg/tasks/feed', MY_SESSION, { time: new Date().getTime() - 300000 })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    done();
                });
        });
    });

    describe('I create a new task and assign it to Alice', () => {
        
        var aliceDefList = -1;

        it('should be able to add a task', (done) => {
            utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { list_id: myBoardId, data: { title: 'Please do this', assigned_to: ALICE.user_id } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    myAddedTask = res.body.keys[0];
                    done();
                });
        });

        it('should be able to read the added task', (done) => {
            utils.doPost(chai, '/tg/tasks/read', MY_SESSION, { id: myAddedTask })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    res.body[0].should.have.property('title').eql('Please do this');
                    done();
                });
        });

        it('Alice should be able to read the added task', (done) => {
            utils.doPost(chai, '/tg/tasks/read', ALICE_SESSION, { id: myAddedTask })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    res.body[0].should.have.property('title').eql('Please do this');
                    done();
                });
        });

        it('should be able to change title', (done) => {
            utils.doPost(chai, '/tg/tasks/edit', MY_SESSION, { id: myAddedTask, list_id: myBoardId, data: { title: 'Please do this baby' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });

        it('Alice also should be able to change title', (done) => {
            utils.doPost(chai, '/tg/tasks/edit', ALICE_SESSION, { id: myAddedTask, list_id: myBoardId, data: { title: 'Please do this baby. Oh boy!' } })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });

        it('Alice should have default list', (done) => {
            utils.doGet(chai, '/tg/lists/my', ALICE_SESSION)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    res.body[0].should.have.property('is_default').eql(true);
                    aliceDefList = res.body[0].list_id;
                    done();
                });
        })

        it('Alice should be able to see it in her default list', done => {
            utils.doPost(chai, '/tg/lists/tasks', ALICE_SESSION, { list_id: aliceDefList })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    done();
                });
        });

        it('Alice should be able to see it in her time line', done => {
            utils.doPost(chai, '/tg/tasks/feed', ALICE_SESSION, { time: new Date().getTime() - 300000 })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                    done();
                });
        });
    })

    after((done) => {
        utils.doPost(chai, '/tg/lists/remove', MY_SESSION, { id: myBoardId })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
    })
});