var encoder     = require('querystring');
var async       = require('async');
var _           = require('lodash');
var Pusher      = require('pusher-client');
var pusher      = new Pusher('3e5aa696821b00d649da', {
    encrypted: true,
    authTransport: 'jsonp',
    authEndpoint: 'http://localhost:8080/pusher/auth',
    auth: {
        headers: {
            'Autho': 'your-user-token'
        }
    }
});

var notifyCallback = function (data) {
    console.log(data);
};

const BASE_URL = 'http://localhost:8080';
const ALICE_DATA = {
    phoneNo: '+94767010789',
    deviceId: 'android-phone'
};

const BOB_USER = {
    phoneNo: '+94777664241',
    deviceId: 'iphone-7'
}

const MARY_USER = {
    phoneNo: '+94772622504',
    deviceId: 'nexus-5'
}

module.exports.BASE_URL = BASE_URL;

module.exports.AUTH_DATA = {
    phoneNo: '+94715245015',
    deviceId: 'abcd'
}

module.exports.USER_DATA = ALICE_DATA;
module.exports.THIRD_USER = BOB_USER;
module.exports.EXTERNAL_USER = MARY_USER;

module.exports.doGet = function (chai, path, sess) {
    var req = chai.request(BASE_URL).get(path);
    if (sess) {
        req.set('cookie', sess);
    }
    return req;
}

module.exports.doPost = function (chai, path, sess, data) {
    var req = chai.request(BASE_URL).post(path);
    if (sess) {
        req.set('cookie', sess);
    }
    return req.send(data || {});
}

module.exports.cleanTables = function (chai, tables, done) {
    chai.request(BASE_URL)
        .post('/_/clean/db')
        .send({ tables: tables })
        .end((err, res) => {
            res.should.have.status(200);
            done();
        });
}

module.exports.readActivationCode = function (chai, phoneNo, deviceId, cb) {
    chai.request(BASE_URL)
        .post('/_/clean/activationKey')
        .send({ phoneNo: phoneNo, deviceId: deviceId })
        .end((err, res) => {
            res.should.have.status(200);
            cb(res.body[0].activation_code);
        });
}

module.exports.readToken = function (chai, phoneNo, deviceId, cb) {
    chai.request(BASE_URL)
        .post('/_/clean/activationKey')
        .send({ phoneNo: phoneNo, deviceId: deviceId })
        .end((err, res) => {
            res.should.have.status(200);
            cb(res.body[0].token);
        });
}

module.exports.userRegister = registerUser;

module.exports.userRegisterAsync = function (data, callback) {
    registerUser(data.chai, data.phoneNo, data.deviceId, uitem => callback(null, uitem));
}

module.exports.openChannel = function (user) {
    // var channel = pusher.subscribe('private-user-' + user.user_id);
    // channel.bind('main', notifyCallback);
    // return channel;
}

module.exports.closeChannels = function (users, channels) {
    // if (_.isArray(users)) {
    //     channels.forEach(function (c) { c.unbind('main', notifyCallback); });
    //     users.forEach(function (u) { pusher.unsubscribe('private-user-' + u.user_id); });
    // } else {
    //     pusher.unsubscribe('private-user-' + users.user_id);
    // }
}

function registerUser(chai, phoneNo, deviceId, cb) {
    var fetch = require('node-fetch');
    var cookie = null;

    fetch(BASE_URL + '/auth/register', {
        method: 'POST',
        body: JSON.stringify({ phoneNo: phoneNo, deviceId: deviceId }),
        headers: { 'Content-Type': 'application/json' }
    })
    .then(response => {
        if (response.status === 401) {
            // a user exist
            return fetch(BASE_URL + '/_/clean/activationKey', {
                method: 'POST',
                body: JSON.stringify({ phoneNo: phoneNo, deviceId: deviceId }),
                headers: { 'Content-Type': 'application/json' }
            }).then(keyRes => { 
                return keyRes.json();
            })
            .then(tkContent => {
                var user = tkContent[0];
                fetch(BASE_URL + '/auth/login', {
                    method: 'POST', body: JSON.stringify({}),
                    headers: { 'Content-Type': 'application/json', 'x-access-token': user.token }
                }).then(loginRes => {
                    var sess = loginRes.headers.raw()['set-cookie'];
                    cb({ user: user, session: sess });
                });
            });

        } else if (response.status === 200) {
            return fetch(BASE_URL + '/_/clean/activationKey', {
                method: 'POST',
                body: JSON.stringify({ phoneNo: phoneNo, deviceId: deviceId }),
                headers: { 'Content-Type': 'application/json' }
            }).then(keyRes => keyRes.json())
            .then(tkContent => {
                var tk = tkContent[0].activation_code;
                fetch(BASE_URL + '/auth/activate', {
                    method: 'POST',
                    body: JSON.stringify({ code: tk, phoneNo: phoneNo, deviceId: deviceId }),
                    headers: { 'Content-Type': 'application/json' }
                }).then(acRes => {
                    fetch(BASE_URL + '/_/clean/activationKey', {
                        method: 'POST',
                        body: JSON.stringify({ phoneNo: phoneNo, deviceId: deviceId }),
                        headers: { 'Content-Type': 'application/json' }
                    })
                    .then(keyRes => keyRes.json())
                    .then(finalTkContent => {
                        var user = finalTkContent[0];
                        fetch(BASE_URL + '/auth/login', {
                            method: 'POST', body: JSON.stringify({}),
                            headers: { 'Content-Type': 'application/json', 'x-access-token': user.token }
                        }).then(loginRes => {
                            var sess = loginRes.headers.raw()['set-cookie'];
                            cb({ user: user, session: sess });
                        });
                    });
                });

            });

        } else {
            return Promise.reject('Unknown error!');
        }
    })
    .catch(err => {
        // user registerted already, let's activate
        console.error(err);
    });
        
}
