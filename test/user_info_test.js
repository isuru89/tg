var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
const async     = require('async');
var fs          = require('fs');
const _         = require('lodash');

const BASE_URL  = utils.BASE_URL;
const _ME      = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.THIRD_USER;

chai.use(chaiHttp);

describe('User Related Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        done();
                    });
    });

    describe('#User Information editing', () => {
        
        describe('I can change my information', (done) => {
            
            it('should be able to change my first and last name', (done) => {
                utils.doPost(chai, '/tg/users/edit', MY_SESSION, { id: ME.user_id, data: { first_name: 'Isuru', last_name: 'Weerarathna' } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('Alice should not be able to change my information', (done) => {
                utils.doPost(chai, '/tg/users/edit', ALICE_SESSION, { id: ME.user_id, data: { first_name: 'Isuru', last_name: 'Weerarathna' } })
                    .end((err, res) => {
                        res.should.have.status(403);
                        res.body.should.have.property('error');
                        done();
                    });
            });

            it('should be able to read my information', done => {
                utils.doPost(chai, '/tg/users/read', MY_SESSION, { id: ME.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('first_name').eql('Isuru');
                        res.body[0].should.have.property('last_name').eql('Weerarathna');
                        res.body[0].should.have.property('mobile_no').eql(ME.mobile_no);
                        done();
                    });
            });

            it('Alice also can read my information except phone number', done => {
                utils.doPost(chai, '/tg/users/read', ALICE_SESSION, { id: ME.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].should.have.property('first_name').eql('Isuru');
                        res.body[0].should.have.property('last_name').eql('Weerarathna');
                        res.body[0].should.not.have.property('mobile_no');
                        res.body[0].should.not.have.property('email');
                        done();
                    });
            });
        });

        describe('User profile image updating', () => {
            it('should be able to change my profile picture', (done) => {
                var bitmap = fs.readFileSync('./test/data/user.jpeg');
                var imgB64 = new Buffer(bitmap).toString('base64');
                utils.doPost(chai, '/tg/users/edit', MY_SESSION, { id: ME.user_id, data: { profile_image: imgB64 } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('should be able to change my profile picture again', (done) => {
                var bitmap = fs.readFileSync('./test/data/user2.jpeg');
                var imgB64 = new Buffer(bitmap).toString('base64');
                utils.doPost(chai, '/tg/users/edit', MY_SESSION, { id: ME.user_id, data: { profile_image: imgB64 } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });
        });
    });

    describe('# User Existing Read', () => {
        it('should be able to lookup my contacts are already in app', (done) => {
            utils.doPost(chai, '/tg/users/lookup', MY_SESSION, { phoneNo: [_ALICE.phoneNo, _BOB.phoneNo] })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).have.lengthOf(2);
                    _.filter(res.body, it => it.user_id < 0).should.have.lengthOf(0);
                    done();
                });
        });

        it('non existence users should return a record too', (done) => {
            utils.doPost(chai, '/tg/users/lookup', MY_SESSION, { phoneNo: [_ALICE.phoneNo, _BOB.phoneNo, '+94098765432'] })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceOf(Array).have.lengthOf(3);
                    _.filter(res.body, it => it.user_id < 0).should.have.lengthOf(1);
                    done();
                });
        });
    });
});