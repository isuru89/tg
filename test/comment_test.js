var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
var _           = require('lodash');
var async       = require('async');

const BASE_URL  = utils.BASE_URL;
const _ME       = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.THIRD_USER;

chai.use(chaiHttp);

describe('Commenting Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        done();
                    });
    });

    describe('#Adding a comment to a task', () => {
        
        var myDefList = -1;
        var myAddedTask = -1;

        describe('I create a new task in default list', (done) => {
            
            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        myDefBoard = res.body[0].list_id;
                        done();
                    });
            });

            it('should be able to create a new task to Alice', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { data: { title: 'Finalize the app design?', assigned_to: ALICE.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('my default list should have the added task', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: myDefList })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        done();
                    });
            });

            it('New task should not have any comment', done => {
                utils.doPost(chai, '/tg/tasks/getComments', MY_SESSION, { task_id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(0);;
                        done();
                    });
            });
        });

        describe('Conversation happens between me and Alice', done => {
            
            it('Alice should be able to add a comment', done => {
                utils.doPost(chai, '/tg/comments/add', ALICE_SESSION, { data: { message: 'Do I need to complete within today?', task: myAddedTask } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('Alice should not have any unread comments', done => {
                utils.doPost(chai, '/tg/comments/unreadCount', ALICE_SESSION, { task_id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].unreadCount.should.be.eql(0);
                        done();
                    });
            });

            it('I should have one unread comment', done => {
                utils.doPost(chai, '/tg/comments/unreadCount', MY_SESSION, { task_id: myAddedTask })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).should.not.be.empty;
                        res.body[0].unreadCount.should.be.eql(1);
                        done();
                    });
            });
        });

    });

});