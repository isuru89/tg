var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');
const async     = require('async');
const _         = require('lodash');

const BASE_URL  = utils.BASE_URL;
const _ME      = utils.AUTH_DATA;
const _ALICE    = utils.USER_DATA;
const _BOB      = utils.THIRD_USER;

chai.use(chaiHttp);

describe('List Sharing Activities', () => {

    var MY_TOKEN = null;
    var ALICE_TOKEN = null;
    var BOB_TOKEN = null;
    var MY_SESSION = null;
    var ALICE_SESSION = null;
    var BOB_SESSION = null;
    var ME = null;
    var ALICE = null;
    var BOB = null;

    before((done) => {
        async.map([ {chai: chai, phoneNo: _ME.phoneNo, deviceId: _ME.deviceId}, 
                    {chai: chai, phoneNo: _ALICE.phoneNo, deviceId: _ALICE.deviceId},
                    {chai: chai, phoneNo: _BOB.phoneNo, deviceId: _BOB.deviceId} ], 
                    utils.userRegisterAsync, 
                    function (err, results) {
                        ME = results[0].user;
                        MY_TOKEN = ME.token;
                        ALICE = results[1].user;
                        ALICE_TOKEN = ALICE.token;
                        BOB = results[2].user;
                        BOB_TOKEN = BOB.token;

                        MY_SESSION = results[0].session;
                        ALICE_SESSION = results[1].session;
                        BOB_SESSION = results[2].session;

                        done();
                    });
    });

    describe('#Empty list sharing', () => {
        
        var myBoardId = -1;
        var myAddedTask = -1;
        var addedListId = -1;

        describe('I create a new list', (done) => {
            
            it('should have only the default board with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            })

            it('Alice should have only the default board with her', (done) => {
                utils.doGet(chai, '/tg/lists/my', ALICE_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            });

            it('Bob should have only the default board with him', (done) => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            });

            it('should be able to create a new list', (done) => {
                utils.doPost(chai, '/tg/lists/add', MY_SESSION, { title: 'my new sharable board' })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('keys').lengthOf(1);
                        addedListId = res.body.keys[0];
                        done();
                    });
            });

            it('should be able to create a new task in new list', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { list_id: addedListId, data: { title: 'Task under new list', assigned_to: ALICE.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('should have two lists under me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        _.filter(res.body, it => it.is_shared === true).should.have.lengthOf(0);
                        done();
                    });
            });

            it('should have the new task added by me', (done) => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

        });

        describe('I share it with Alice', (done) => {

            it('should be able to share it with Alice', (done) => {
                utils.doPost(chai, '/tg/lists/share', MY_SESSION, { list_id: addedListId, user_id: ALICE.user_id })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('success').eql(true);
                            done();
                        });
            });

            it('Alice now has two lists', (done) => {
                utils.doGet(chai, '/tg/lists/my', ALICE_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        _.filter(res.body, it => it.is_shared === true).should.have.lengthOf(1);
                        done();
                    });
            });

            it('list should be marked as shared', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        _.filter(res.body, it => it.is_shared === true).should.have.lengthOf(1);
                        done();
                    });
            });

            it('Bob still have only the default list with him', (done) => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            })

            it('shared list now has two users, me and Alice', (done) => {
                utils.doPost(chai, '/tg/lists/sharedUsers', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('user_id').eql(ALICE.user_id);
                        done();
                    });
            });

            it('should still see the new task added by me', (done) => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

            it('Alice should see the new task added by me', (done) => {
                utils.doPost(chai, '/tg/lists/tasks', ALICE_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });
        });

        describe('I share it with Bob too', (done) => {

            it('Bob still have only the default board with him', (done) => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            });

            it('should be able to share it with Bob', (done) => {
                utils.doPost(chai, '/tg/lists/share', MY_SESSION, { list_id: addedListId, user_id: BOB.user_id })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('success').eql(true);
                            done();
                        });
            });

            it('Bob now has two boards', (done) => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        done();
                    });
            });

            it('shared list now has three users, me, Bob and Alice', (done) => {
                utils.doPost(chai, '/tg/lists/sharedUsers', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        done();
                    });
            });

            it('should still see the new task added by me', (done) => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

            it('Bob should see the new task added by me', (done) => {
                utils.doPost(chai, '/tg/lists/tasks', BOB_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });
        });

        describe('I remove Alice from shared list', (done) => {

            it('should be able to remove Alice from shared list', (done) => {
                utils.doPost(chai, '/tg/lists/removeSharedUser', MY_SESSION, { list_id: addedListId, user_id: ALICE.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('shared list now only have me and Bob', (done) => {
                utils.doPost(chai, '/tg/lists/sharedUsers', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

            it('Alice again now have only one board', (done) => {
                utils.doGet(chai, '/tg/lists/my', ALICE_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

            it('should be able to remove Bob from shared list', (done) => {
                utils.doPost(chai, '/tg/lists/removeSharedUser', MY_SESSION, { list_id: addedListId, user_id: BOB.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('Bob again now have only one board', (done) => {
                utils.doGet(chai, '/tg/lists/my', BOB_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        done();
                    });
            });

            it('shared list now only have me', (done) => {
                utils.doPost(chai, '/tg/lists/sharedUsers', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(0);
                        done();
                    });
            });

        });

        describe('I delete the created list', done => {
            it('should be able to remove list', done => {
                utils.doPost(chai, '/tg/lists/remove', MY_SESSION, { id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            })
        });
    });

    describe('#List with task sharing', () => {
        var addedListId = -1;
        var myAddedTask = -1;

        describe('I create a new list', done => {
            it('should have only the default list with me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        done();
                    });
            });

            it('should be able to create a new list', (done) => {
                utils.doPost(chai, '/tg/lists/add', MY_SESSION, {  title: 'my new sharable board' })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('keys').lengthOf(1);
                        addedListId = res.body.keys[0];
                        done();
                    });

            });

            it('should have two boards under me', (done) => {
                utils.doGet(chai, '/tg/lists/my', MY_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(2);
                        //res.body.should.have.property('success').eql(true);
                        done();
                    });
            });
        });

        describe('Adding a new task to it', done => {
            var aliceDefList = -1;

            it('should not have any tasks under new list', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(0);
                        done();
                    });
            });

            it('create a new task and assign it to alice', done => {
                utils.doPost(chai, '/tg/tasks/add', MY_SESSION, { list_id: addedListId, data: { title: 'Progress meeting @ 2pm', assigned_to: ALICE.user_id } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        myAddedTask = res.body.keys[0];
                        done();
                    });
            });

            it('should have one task under new list', done => {
                utils.doPost(chai, '/tg/lists/tasks', MY_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('title').eql('Progress meeting @ 2pm');
                        res.body[0].should.have.property('assigned_to').eql(ALICE.user_id);
                        done();
                    });
            });

            it('Alice should have only the default board with her', (done) => {
                utils.doGet(chai, '/tg/lists/my', ALICE_SESSION)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('is_default').eql(true);
                        aliceDefList = res.body[0].list_id;
                        done();
                    });
            });
        });

        describe('share the list with Alice', done => {
            it('should be able to share it with Alice', done => {
                utils.doPost(chai, '/tg/lists/share', MY_SESSION, { list_id: addedListId, user_id: ALICE.user_id })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

            it('Alice can view the task under the shared list', done => {
                utils.doPost(chai, '/tg/lists/tasks', ALICE_SESSION, { list_id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.instanceOf(Array).and.have.lengthOf(1);
                        res.body[0].should.have.property('title').eql('Progress meeting @ 2pm');
                        res.body[0].should.have.property('assigned_to').eql(ALICE.user_id);
                        res.body[0].should.have.property('created_by').eql(ME.user_id);
                        done();
                    });
            });

            it("Alice can't edit the title", done => {
                utils.doPost(chai, '/tg/lists/edit', ALICE_SESSION, { id: addedListId, data: { title: 'Project Progress meeting @ 2pm' } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(false);
                        done();
                    });
            });

            it("Alice can't delete the list", done => {
                utils.doPost(chai, '/tg/lists/remove', ALICE_SESSION, { id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(false);
                        done();
                    });
            });

            it("as an owner, I can edit the title", done => {
                utils.doPost(chai, '/tg/lists/edit', MY_SESSION, { id: addedListId, data: { title: 'Project Progress meeting @ 2pm' } })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });

        });

        describe('delete the list', done => {
            it("as an owner, I can delete the list", done => {
                utils.doPost(chai, '/tg/lists/remove', MY_SESSION, { id: addedListId })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        done();
                    });
            });
        });
    });


});