process.env.NODE_ENV = 'test';

var chai        = require('chai');
var chaiHttp    = require('chai-http');
var should      = chai.should();
var utils       = require('./utils');

chai.use(chaiHttp);

const BASE_URL  = utils.BASE_URL;
const ADATA     = utils.AUTH_DATA;

describe('First Time User', () => {

    var userToken;

    // clean db before all cases
    before((done) => utils.cleanTables(chai, ['list', 'user_device', 'user'], done));

    describe('POST /auth/register', () => {
        it('should be able to register with a phone no and device id', (done) => {
            chai.request(BASE_URL)
                .post('/auth/register')
                .send({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                }); 
        }).timeout(10000);

        it('sending the same phone no and device before activation should send a new code', (done) => {
            chai.request(BASE_URL)
                .post('/auth/register')
                .send({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });

        it('should fail registration without phone no', (done) => {
            chai.request(BASE_URL)
                .post('/auth/register')
                .send({ deviceId: ADATA.deviceId })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('error');
                    done();
                });
        });

        it('should fail registration without devide id', (done) => {
            chai.request(BASE_URL)
                .post('/auth/register')
                .send({ phoneNo: ADATA.phoneNo })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('error');
                    done();
                });
        });
    });

    describe('GET /auth/activate', () => {
        it('should not be able to activate using incorrect code', (done) => {
            chai.request(BASE_URL)
                .get('/auth/activate')
                .query({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId, code: new Date().getTime().toString() })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.have.property('error');
                    done();
                }); 
        });

        it('should not be able to activate using non-existing phone number', (done) => {
            chai.request(BASE_URL)
                .get('/auth/activate')
                .query({ phoneNo: '+9412345678', deviceId: ADATA.deviceId, code: new Date().getTime().toString() })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.have.property('error');
                    done();
                }); 
        });

        it('should not be able to activate using non-existing device id', (done) => {
            chai.request(BASE_URL)
                .get('/auth/activate')
                .query({ phoneNo: ADATA.phoneNo, deviceId: 'abcdefg', code: new Date().getTime().toString() })
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.have.property('error');
                    done();
                }); 
        });

        it('should be failed when no phone number provided', (done) => {
            chai.request(BASE_URL).get('/auth/activate')
                .query({ deviceId: ADATA.deviceId, code: new Date().getTime().toString() })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('error');
                    done();
                });  
        });

        it('should be failed when no device id provided', (done) => {
            chai.request(BASE_URL).get('/auth/activate')
                .query({ phoneNo: ADATA.phoneNo, code: new Date().getTime().toString() })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('error');
                    done();
                });  
        });

        it('should be failed when no code is provided', (done) => {
            chai.request(BASE_URL).get('/auth/activate')
                .query({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('error');
                    done();
                });  
        });

        it('should be able to activate using correct code', (done) => {
            utils.readActivationCode(chai, ADATA.phoneNo, ADATA.deviceId, (code) => {
                chai.request(BASE_URL)
                    .get('/auth/activate')
                    .query({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId, code: code })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('token');
                        userToken = res.body.token;
                        done();
                    }); 
            });
        });

        it('should be able to activate using the same code again', (done) => {
            utils.readActivationCode(chai, ADATA.phoneNo, ADATA.deviceId, (code) => {
                chai.request(BASE_URL)
                    .get('/auth/activate')
                    .query({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId, code: code })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('success');
                        done();
                    }); 
            });
        });
    });

    // describe('POST /auth/reset', () => {
    //     var rToken;
    //     var newToken;

    //     it('should be able to get reset code for the activated device', done => {
    //         chai.request(BASE_URL)
    //             .post('/auth/register')
    //             .send({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId })
    //             .end((err, res) => {
    //                 res.should.have.status(401);
    //                 res.body.should.have.property('resetToken');
    //                 rToken = res.body.resetToken;
    //                 done();
    //             });  
    //     });

    //     it('should be able to reset for new token', done => {
    //         chai.request(BASE_URL)
    //             .post('/auth/reset')
    //             .send({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId, token: rToken })
    //             .end((err, res) => {
    //                 res.should.have.status(200);
    //                 res.body.should.have.property('success').eql(true);
    //                 done();
    //             }); 
    //     });

    //     it('should be able to register again', done => {
    //         chai.request(BASE_URL)
    //             .post('/auth/register')
    //             .send({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId })
    //             .end((err, res) => {
    //                 res.should.have.status(200);
    //                 res.body.should.have.property('success');
    //                 done();
    //             });  
    //     });

    //     it('should be able to activate using new code', (done) => {
    //         utils.readActivationCode(chai, ADATA.phoneNo, ADATA.deviceId, (code) => {
    //             chai.request(BASE_URL)
    //                 .get('/auth/activate')
    //                 .query({ phoneNo: ADATA.phoneNo, deviceId: ADATA.deviceId, code: code })
    //                 .end((err, res) => {
    //                     res.should.have.status(200);
    //                     res.body.should.have.property('success').eql(true);
    //                     res.body.should.have.property('token');
    //                     newToken = res.body.token;
    //                     done();
    //                 }); 
    //         });
    //     });

    //     it('returned token should not be equal to old one', done => {
    //         if (rToken != userToken) {
    //             done();
    //         }
    //     });

    // }); 

    //after((done) => utils.cleanTables(chai, ['user_device', 'user'], done));
    
});
