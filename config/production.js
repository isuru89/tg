module.exports = {

    port: process.env.APP_PORT,

    appSecret: process.env.APP_KEY_SECRET,

    services: {
        urlShortnerApiKey: process.env.APP_URL_SHORT_KEY,
        analyticsEnabled: process.env.APP_ANALYTICS_ENABLED,
        analyticsKey: process.env.APP_ANALYTICS_KEY
    },

    ny: {
        url: 'http://nyserver:9009/ny',
        token: process.env.NYSERVER_AUTH_TOKEN
    },

    alerts: {
        pusher: {
            enabled: true,
            appId: process.env.APP_PUSHER_ID,
            pusherKey: process.env.APP_PUSHER_KEY,
            pusherSecret: process.env.APP_PUSHER_SECRET
        },
        sms: {
            accountSID: process.env.APP_TWILIO_SMS_SID,
            authToken: process.env.APP_TWILIO_SMS_AUTH_TOKEN,
            sendingNumber: process.env.APP_TWILIO_SMS_SEND_NO
        }
    },

    sessions: {
        secret: process.env.APP_SESSION_SECRET
    },

    redis: {
        url: 'redis://redis'
    }
}