module.exports = {

    appName: 'TaskGraph',

    host: process.env.APP_HOST_URL || 'http://localhost',
    port: process.env.APP_PORT || 8080,

    appSecret:     process.env.APP_KEY_SECRET || 'thisistopsecretappkey', 
    
    services: {
        urlShortnerApiKey: process.env.APP_URL_SHORT_KEY || 'xxxAIzaSyBTgbLFzT5zPO1ofmhjVe11Q7zf-pwLpB8',
        analyticsEnabled: false,
        analyticsKey: process.env.APP_ANALYTICS_KEY || 'UA-92076262-1'
    },

    ny: {
        url: process.env.APP_NY_URL || 'http://localhost:9009/ny',
        token: process.env.NYSERVER_AUTH_TOKEN || '5d7900ccb78949de97804d8f9b596728'
    },

    alerts: {
        pusher: {
            enabled: false,
            appId: '',
            pusherKey: '',
            pusherSecret: ''
        },
        sms: {
            accountSID: process.env.APP_TWILIO_SMS_SID || 'AC7e01f92257a46a337f19754281fc2e03',
            authToken: process.env.APP_TWILIO_SMS_AUTH_TOKEN || '873d3264391f19d346283d81df3a3a95',
            sendingNumber: process.env.APP_TWILIO_SMS_SEND_NO || '5005550006'
        }
    },

    sessions: {
        secret: process.env.APP_SESSION_SECRET || 'thisiscookiesecret'
    },

    // refer possible options here http://redis.js.org/#api-rediscreateclient
    redis: {
        url: process.env.APP_REDIS_URL || 'redis://localhost'
    }
}